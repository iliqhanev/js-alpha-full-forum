import { UpdatePost } from './../../common/update-post';
import { CreatePost } from './../../common/create-post';
import { Post } from '../../common/post';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostService {


  constructor(
    private readonly http: HttpClient,
    ) { }

  public allPosts(): Observable<{posts: Post[]}> {
    return this.http.get<{posts: Post[]}>('http://localhost:3000/posts');
  }
  public getPostById(postId: string): Observable<{post: Post}> {
    return this.http.get<{post: Post}>(`http://localhost:3000/posts/${postId}`);
  }

  public createPost(post: CreatePost): Observable<{post: Post}> {
    return this.http.post<{post: Post}>('http://localhost:3000/posts', { ...post });
}

public deletePostById(postId: string): Observable<{post: Post}> {
  return this.http.delete<{post: Post}>(`http://localhost:3000/posts/${postId}`);
}

public updatePostById(postId: string, post: UpdatePost): Observable<{post: Post}> {
  return this.http.put<{post: Post}>(`http://localhost:3000/posts/${postId}`, {...post});
}
}
