import { Comment } from './../../common/comment';
// /* tslint:disable:no-unused-variable */

// import { TestBed, async, inject } from '@angular/core/testing';
// import { CommentService } from './comment.service';

// describe('Service: Comment', () => {
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       providers: [CommentService]
//     });
//   });

//   it('should ...', inject([CommentService], (service: CommentService) => {
//     expect(service).toBeTruthy();
//   }));
// });

import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of, from } from 'rxjs';
import { CommentService } from './comment.service';

describe('CommentService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: HttpClient,
                    useValue: http,
                },
            ]
        });
    });

    it('Comment service should be defined', () => {
        const service: CommentService = TestBed.get(CommentService);

        expect(service).toBeTruthy();
    });




    it('all comments by post should return array of comment objects', () => {
      // arraing
        const service: CommentService = TestBed.get(CommentService);
        http.get.and.returnValue(of([
          {id: '1'},
          {id: '2'}
        ]));

        service.allCommentsByPost('').subscribe(
            res => {
                expect(res[0].id).toEqual('1');
                expect(res[1].id).toEqual('2');
            }
        );
    });

    it('all Comments should call http.get once', () => {
        const service: CommentService = TestBed.get(CommentService);

        http.get.calls.reset();

        service.allCommentsByPost('').subscribe(
            () => expect(http.get).toHaveBeenCalledTimes(1)
        );
    });

    it('create comment should create a new comment', () => {
        const service: CommentService = TestBed.get(CommentService);
        const comment = {
            content: 'test'
        };
        http.post.and.returnValue(of(comment));

        service.createCommentToPost('', comment).subscribe(
            (res: any) => expect(res.content).toEqual('test')
        );
    });

    it('Create Comment should call http.post once', () => {
        const service: CommentService = TestBed.get(CommentService);
        const comment = {
            content: 'test2'
        };
        http.post.calls.reset();

        service.createCommentToPost('', comment).subscribe(
            () => expect(http.post).toHaveBeenCalledTimes(1)
        );
    });



    // it('Create comment should throw error when invalid arguments are provided', () => {
    //     const service: CommentService = TestBed.get(CommentService);

    //     service.createCommentToPost(null, null).subscribe(
    //         (res) => expect(res).toThrowError('')
    //     );
    // });

    it('Update comment should update a comment', () => {
      const service: CommentService = TestBed.get(CommentService);
      const comment = {
          content: 'test'
      };
      http.put.and.returnValue(of(comment));

      service.updateComment('', '', comment).subscribe(
          (res: any) => expect(res.content).toEqual('test')
      );
  });

  it('Create Comment should call http.post once', () => {
      const service: CommentService = TestBed.get(CommentService);
      const comment = {
          content: 'test'
      };
      http.put.calls.reset();

      service.updateComment('', '', comment).subscribe(
          () => expect(http.put).toHaveBeenCalledTimes(1)
      );
  });

  it('Delete comment should delete a comment', () => {
    const service: CommentService = TestBed.get(CommentService);
    const comment = {
        content: 'test'
    };
    http.delete.and.returnValue(of(comment));

    service.deleteCommentById('', '').subscribe(
        (res: any) => expect(res.content).toEqual('test')
    );
});

it('Delete Comment should call http.post once', () => {
    const service: CommentService = TestBed.get(CommentService);
    const comment = {
        content: 'test'
    };
    http.delete.calls.reset();

    service.deleteCommentById('', '').subscribe(
        () => expect(http.delete).toHaveBeenCalledTimes(1)
    );
});











});
