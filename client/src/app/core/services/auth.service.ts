import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userSubject$ = new BehaviorSubject<string | null>(this.username);

  constructor(
    private readonly storage: StorageService,
    private readonly http: HttpClient,
  ) { }

  public get user$(): Observable<string | null> {
    return this.userSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';
    if (token) {
      return username;
    }
    return null;
  }

  // // TODO
  // public get username(): string | null {
  //   const token = this.storage.get('token');
  //   const username =  this.storage.get('username');
  //   if (token && username) {
  //     return username;
  //   }
  //   this.logout();
  //   return null;
  // }

  public tokenPeyloadLogger(): any {
    const token = this.storage.get('token');
    const decoded = jwt_decode(token);
    console.log(decoded);
    return decoded;
    // return token;
  }

  public tokenPeyloadReader(token: string): any {
    const decoded = jwt_decode(token);
    // console.log(decoded);
    return decoded;
  }

  public register(name: string, password: string, email: string) {
    const res = this.http.post('http://localhost:3000/auth', {
      name,
      password,
      email,
    });

    return res;
  }

  public login(name: string, password: string, email: string) {

    return this.http
      .post('http://localhost:3000/auth/session', {
        name,
        password,
        email,
      })
      .pipe(
        tap((res: any) => {
          // console.log(res);

          const payload: any = this.tokenPeyloadReader(res.token);
          // console.log(payload);

          this.userSubject$.next(payload.name);
          this.storage.set('role', res.yourAccessLevel);
          this.storage.set('token', res.token);
          this.storage.set('username', payload.name);

        })
      );
  }

  public logout(): void {
    this.storage.remove('token');
    this.storage.remove('username');
    this.storage.remove('role');
    this.userSubject$.next(null);
  }
}
