import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateComment } from './../../../app/common/create-comment';
import { HttpClient } from '@angular/common/http';
import { UpdateComment } from './../../../app/common/update-comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
    private readonly http: HttpClient,
    ) { }

  public allCommentsByPost(postId: string): Observable<{comments: Comment[]}> {
    return this.http.get<{comments: Comment[]}>(`http://localhost:3000/posts/${postId}/comments`);
  }

  public createCommentToPost(postId: string, comment: CreateComment): Observable<{comment: Comment}> {
    return this.http.post<{comment: Comment}>(`http://localhost:3000/posts/${postId}/comments`, { ...comment });
}

public updateComment(postId: string, commentId: string, comment: UpdateComment): Observable<{comment: Comment}> {
  return this.http.put<{comment: Comment}>(`http://localhost:3000/posts/${postId}/comments/${commentId}`, {...comment});
}

public deleteCommentById(postId: string, commentId: string): Observable<{comment: Comment}> {
  return this.http.delete<{comment: Comment}>(`http://localhost:3000/posts/${postId}/comments/${commentId}`);
}


}
