import { TestBed } from '@angular/core/testing';
import { PostService } from './post.service';
import { HttpClient } from '@angular/common/http';
import { of, from } from 'rxjs';

describe('PostService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: HttpClient,
                    useValue: http,
                },
            ]
        });
    });

    it('service should be defined', () => {
        const service: PostService = TestBed.get(PostService);

        expect(service).toBeTruthy();
    });

    it('allPosts should return array of post objects', () => {
        const service: PostService = TestBed.get(PostService);
        http.get.and.returnValue(of({}));

        service.allPosts().subscribe(
            res => {
                expect(res).toBeTruthy();
            }
        );
    });

    it('allPosts should call http.get once', () => {
        const service: PostService = TestBed.get(PostService);
        http.get.calls.reset();

        service.allPosts().subscribe(
            () => expect(http.get).toHaveBeenCalledTimes(1)
        );
    });

    it('getPostById should return a specific post by id', () => {
        const service: PostService = TestBed.get(PostService);
        http.get.and.returnValue(of({
            post: {
                id: 'test'
            }
        }));

        service.getPostById('test').subscribe(
            (res: any) => {
                expect(res.post.id).toBe('test');
            }
        );
    });

    it('getPostById should call http.get once', () => {
        const service: PostService = TestBed.get(PostService);
        http.get.calls.reset();

        service.allPosts().subscribe(
            () => expect(http.get).toHaveBeenCalledTimes(1)
        );
    });

    it('createPost should create a new post', () => {
        const service: PostService = TestBed.get(PostService);
        const post = {
            title: 'test',
            content: 'test'
        };
        http.post.and.returnValue(of(post));

        service.createPost(post).subscribe(
            (res: any) => expect(res.title).toEqual('test')
        );
    });

    it('createPost should call http.post once', () => {
        const service: PostService = TestBed.get(PostService);
        const post = {
            title: 'test',
            content: 'test'
        };
        http.post.calls.reset();

        service.createPost(post).subscribe(
            () => expect(http.post).toHaveBeenCalledTimes(1)
        );
    });

    // it('createPost should throw error when invalid arguments are provided', () => {
    //     const service: PostService = TestBed.get(PostService);

    //     service.createPost(null).subscribe(
    //         (res) => expect(res).toThrow()
    //     );
    // });
});
