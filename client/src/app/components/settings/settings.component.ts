import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { HttpClient } from '@angular/common/http';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  public isAdmin: any;
  public isAuthor: any;
  public test: boolean;
  @Input() isInCommentComponent: boolean;
  @Input() post: any;
  constructor(
    private readonly storage: StorageService,
    private readonly http: HttpClient,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.isAuthor = this.post.author === this.storage.get('username');
    this.test = this.isInCommentComponent;
    this.isAdmin = this.storage.get('role').includes('Admin');
  }

  lockPost() {
    return this.http.delete(`http://localhost:3000/posts/lock/${this.post.id}`).subscribe(
      (data) => {
        this.notificator.success(`You have locked the following post for changes : ${this.post.title}`);
      }
    );
  }

  flagPost() {
    return this.http.post(`http://localhost:3000/posts/flagPost/${this.post.id}`, '').subscribe(
      (data) => {
        this.notificator.success(`You have locked the following post for changes : ${this.post.title}`);
      }
    );
  }

  public deletePost() {
    return this.http.delete(`http://localhost:3000/posts/${this.post.id}`).subscribe(
      () => {
        this.notificator.success(`You have removed the following post: ${this.post.title}`);
      }
    );
  }

  public deleteComment() {
    return this.http.delete(`http://localhost:3000/comments/${this.post.id}`).subscribe(
      (data) => {
        console.log(data);

        this.notificator.success(`You have succesfully removed the comment`);
      }
    );
  }

  public likeOrDislikePost(voteValue) {


    if (!!this.post.title) {
      // yet there's better solution,maybe to emit values and handle on smart component to like/dislike
      // either create like/dislike isolated component
      // if this.post.id is true means that you clicked on a post, otherwise you've clicked on a comment
      // so the http will handle another post request and update the comment likes/dislikes

      return this.http.post(`http://localhost:3000/posts/vote/${this.post.id}/${voteValue}`, '').subscribe(
        () => {
          this.notificator.success(`You liked the following post: ${this.post.title}`);
        }
      );
    } else {
      return this.http.post(`http://localhost:3000/comments/${this.post.id}/${voteValue}`, '').subscribe(
        (data) => {
          this.notificator.success(`You liked the following comment: ${this.post.content}`);
        }
      );
    }
  }
}
