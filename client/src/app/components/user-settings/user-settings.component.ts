import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { HttpClient } from '@angular/common/http';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Subscription, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/common/post';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit, OnDestroy {

  @Input() currentUser: any;
  @Input() post: any;
  @Input() public posts: Post[];

  public isAuthor: any;
  public user$: Subscription;
  public isAdmin: any;

  constructor(
    private readonly storage: StorageService,
    private readonly http: HttpClient,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.isAdmin = this.storage.get('role') === 'Admin';
  }

  ngOnDestroy() {
  }

  public addFriend() {
    return this.http.post(`http://localhost:3000/users/friends/${this.currentUser}`, '').subscribe(
      (data) => {
        this.notificator.success(`You succesfully started to follow: ${this.currentUser}`);
      }
    );
  }

  public unFriend() {
    return this.http.delete(`http://localhost:3000/users/friends/${this.currentUser}`).subscribe(
      (data) => {
        this.notificator.success(`You no longer follow: ${this.currentUser}`);
      }
    );
  }

  public deleteUser() {
    return this.http.delete(`http://localhost:3000/users/deleteuser/${this.currentUser}`).subscribe(
      (data) => {
        this.notificator.success(`You have banned the following user: ${this.currentUser}`);
      }
    );
  }

  public banUser() {
    return this.http.delete(`http://localhost:3000/users/ban/${this.currentUser}`).subscribe(
      (data) => {
        this.notificator.success(`You have banned the following user: ${this.currentUser}`);
      }
    );
  }
}
