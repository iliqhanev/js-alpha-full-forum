import { TestBed, async } from '@angular/core/testing';
import { NotificatorService } from '../../core/services/notificator.service';
import { StorageService } from '../../core/services/storage.service';
import { UserSettingsComponent } from './user-settings.component';
import { HttpClient } from 'selenium-webdriver/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { CoreModule } from 'src/app/core/core.module';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { RegisterComponent } from '../register/register.component';
import { ProfileComponent } from '../profile/profile.component';
import { PostsComponent } from 'src/app/posts/posts/posts.component';
import { ShowPostComponent } from 'src/app/posts/show-post/show-post.component';
import { ShowCommentComponent } from 'src/app/posts/show-comment/show-comment.component';
import { CreatePostComponent } from 'src/app/posts/create-post/create-post.component';
import { PostsModule } from 'src/app/posts/posts.module';
import { AppModule } from 'src/app/app.module';

describe('UserSettingsComponent', () => {
    let fixture;
    const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
    const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                AppModule,
                AppRoutingModule,
                PostsModule,
                CoreModule
            ],
            providers: [
                {
                    provide: NotificatorService,
                    useValue: notificator,
                },
                {
                    provide: StorageService,
                    useValue: storage,
                },
                {
                    provide: HttpClient,
                    useValue: http,
                },
            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the app', () => {
        fixture = TestBed.createComponent(UserSettingsComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('#isAdmin should be false if admin has not logged', async () => {
        fixture = TestBed.createComponent(UserSettingsComponent);
        const app: UserSettingsComponent = fixture.debugElement.componentInstance;

        await fixture.detectChanges();
        expect(app.isAdmin).toBe(false);
    });
});
