import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input()
  public readonly loggedIn: boolean;

  @Input()
  public readonly username: string;

  @Output()
  public toggle = new EventEmitter<undefined>();

  @Output()
  public toggleLogout = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  public toggleMenu(): void {
    this.toggle.emit();
  }

  public triggerLogout() {
    this.toggleLogout.emit();
  }

}
