import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  triggerRegister(name: string, password: string, email: string) {
    this.auth.register(name, password, email).subscribe(
      result => {
        this.notificator.success('You have registered successfully!');
        this.auth.login(name, password, email).subscribe(
          res => {
            // this.notificator.success(`Wellcome ${res.user.name}`);
            this.notificator.success(`Wellcome ${name}`);
            this.router.navigate(['home']);
          },
          error => this.notificator.error(error.message),
        );
      },
      error => this.notificator.error(error.message),
    );
  }

}
