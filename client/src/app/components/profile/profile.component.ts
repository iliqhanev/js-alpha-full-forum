import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public isAdmin: boolean;
  public friends: any;
  public myPosts: any;
  public flaggedPosts: any;
  public lockedPosts: any;
  public user: any;

  constructor(
    private readonly http: HttpClient,
    private readonly notificator: NotificatorService,
    private readonly storage: StorageService
  ) { }

  ngOnInit() {
    // const currentUser = this.storage.get('username');

    this.isAdmin = this.storage.get('role').includes('Admin');
    this.user = this.storage.get('username');
    this.http.get(`http://localhost:3000/users`).subscribe(
      (data: any) => {
        this.friends = data.yourFriends;
      }
    );

    this.http.get(`http://localhost:3000/posts`).subscribe(
      (data: any) => {
        this.myPosts = data.filter(post => post.author === this.user);
        this.flaggedPosts = data.filter(post => post.isFlagged === true);
        this.lockedPosts = data.filter(post => post.isLocked === true);
      }
    );
  }

}
