import { Injectable } from '@angular/core';
import { NotificatorService } from './../../../app/core/services/notificator.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './../../../app/core/services/comment.service';

@Injectable({
  providedIn: 'root'
})
export class CommentsResolverService {
  constructor(
    private readonly commentService: CommentService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const postId = route.params['postId'];
    return this.commentService.allCommentsByPost(postId)
      .pipe(
        catchError(
        err => {
          // TODO handle error
          this.notificator.error(err.error);
          console.log(err);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of({comments: []});
        }
      ));
  }

}
