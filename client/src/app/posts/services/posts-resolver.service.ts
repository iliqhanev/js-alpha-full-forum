import { Injectable } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsResolverService {

  constructor(
    private readonly postService: PostService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.postService.allPosts()
      .pipe(
        catchError(
        err => {
          // TODO handle error
          this.notificator.error(err.error);
          console.log(err);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of({posts: []});
        }
      ));
  }
}
