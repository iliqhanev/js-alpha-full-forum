import { Post } from './../../common/post';
import { PostService } from './../../core/services/post.service';
import { CreatePost } from './../../common/create-post';
import { Component, OnInit } from '@angular/core';
import { NotificatorService } from './../../../app/core/services/notificator.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})

export class CreatePostComponent implements OnInit {

  createPostForm;

  constructor(
    private formBuilder: FormBuilder,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly posts: PostService,
  ) { }

  ngOnInit() {

    this.createPostForm = this.formBuilder.group({
      title: '',
      content: ''
    });
  }

  onSubmit(value) {

    this.posts.createPost(value)
   .subscribe(
      res => {
      this.notificator.success(`Post created`);
          this.router.navigate(['posts']);
          },
          err => {
              this.notificator.error(err.message);
            }
          );
  }

  close(event) {
    event.stopPropagation();
    this.router.navigate(['posts']);

  }

}
