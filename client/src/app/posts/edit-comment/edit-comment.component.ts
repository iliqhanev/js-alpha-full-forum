import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificatorService } from './../../../app/core/services/notificator.service';
import { CommentService } from './../../../app/core/services/comment.service';
import { catchError } from 'rxjs/operators';
import { Comment } from './../../../app/common/comment';



@Component({
  selector: 'app-edit-comment',
  templateUrl: './edit-comment.component.html',
  styleUrls: ['./edit-comment.component.css'],
})
export class EditCommentComponent implements OnInit {


  updateCommentForm: FormGroup;
  subscription: Subscription;
  @Input() comment: Comment;
  postId: string;
  @Output() notifyParent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() updatedComment: EventEmitter<Comment | {}> = new EventEmitter<Comment | {}>();

  constructor(
    private readonly route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
   private readonly commentService: CommentService) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.postId =  params.get('postId');
    });

    this.updateCommentForm = this.formBuilder.group({
      content:  ['',  [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    });


    this.updateCommentForm.controls['content'].setValue(this.comment.content);
  }

  onSubmit() {
    console.log(this.comment);
    this.subscription = this.commentService.updateComment(this.postId, this.comment.id, this.updateCommentForm.value)
    .subscribe(
      res => {
      this.notificator.success(`Comment is updated`);
      this.notifyParent.emit(true);
      this.updatedComment.emit(res);
          },
          err => {
              this.notificator.error(err.message);
              this.notifyParent.emit(false);
            });

 }

  close(event) {
    event.stopPropagation();
    this.router.navigate(['posts']);

  }
}
