import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowPostComponent } from './show-post/show-post.component';
import { ShowCommentComponent } from './show-comment/show-comment.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import { PostsComponent } from './posts/posts.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EditPostComponent } from './edit-post/edit-post.component';
import { EditCommentComponent } from './edit-comment/edit-comment.component';
@NgModule({
  declarations: [PostsComponent,
    ShowPostComponent,
    ShowCommentComponent,
    CreatePostComponent,
    CreateCommentComponent,
    EditPostComponent,
    EditCommentComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class PostsModule { }
