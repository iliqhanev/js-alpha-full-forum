import { Comment } from './../../common/comment';
import { ActivatedRoute, Params } from '@angular/router';
import { EditCommentComponent } from './../edit-comment/edit-comment.component';
import { CommentService } from 'src/app/core/services/comment.service';
import { ShowCommentComponent } from './show-comment.component';

import { TestBed, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { ReactiveFormsModule } from '@angular/forms';
import { of, Observable } from 'rxjs';

class MockActivatedRoute {
  params = {
    subscribe: jasmine.createSpy('subscribe')
     .and
     .returnValue(of(<Params>{postId: 1}))
  };
}
describe('Show-comment.component', () => {
    let fixture;
    const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);

    const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);
    const commentService = jasmine.createSpyObj('CommentService', {'deleteCommentById': of(Comment)});


    beforeEach(async(() => {
        TestBed.configureTestingModule({

          declarations: [ShowCommentComponent,
            EditCommentComponent],
            imports: [
              CommonModule,
              SharedModule,
              ReactiveFormsModule,
            ],
            providers: [
                {
                    provide: NotificatorService,
                    useValue: notificator,
                },
                {
                    provide: StorageService,
                    useValue: storage,
                },
                {
                  provide: CommentService,
                  useValue: commentService,
                },

                { provide: ActivatedRoute, useValue: new MockActivatedRoute() }

            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the show-comment component', () => {
        fixture = TestBed.createComponent(ShowCommentComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });


  //   it('onInit() should set comment.isAuthorOrAdmin of true/false if is/not author; update  component if user is author', () => {
  //       storage.get('username').and.returnValue(of('test'));
  //       storage.get('role').and.returnValue(of(''));

  //     fixture = TestBed.createComponent(ShowCommentComponent);
  //     const component = fixture.debugElement.componentInstance;

  //     component.comments = [ {id: '1', autor: 'test'},
  //     {id: '2', autor: 'test2'}]
  //     component.detectChanges();
  //     expect(component.comments[0].isAuthorOrAdmin).toEqual(true);
  //     expect(component.comments[1].isAuthorOrAdmin).toEqual(false);

  // });


 });
