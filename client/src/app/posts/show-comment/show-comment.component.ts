import { CommentService } from './../../../app/core/services/comment.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { Comment } from './../../../app/common/comment';
import { NotificatorService } from './../../../app/core/services/notificator.service';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from './../../../app/core/services/storage.service';

@Component({
  selector: 'app-show-comment',
  templateUrl: './show-comment.component.html',
  styleUrls: ['./show-comment.component.css']
})

export class ShowCommentComponent implements OnInit {

  @Input() comments: Comment[];
 postId: string;
  editMode = false;
  showCreateComment = false;
  subscription: Subscription;

  constructor(
    private readonly storage: StorageService,
    private readonly route: ActivatedRoute,
    private readonly commentService: CommentService,
    private readonly notificator: NotificatorService,

  ) { }

  ngOnInit() {
  const name = this.storage.get('username');
  const role = this.storage.get('role');

    this.route.paramMap.subscribe(params => {
      this.postId =  params.get('postId');
    });

    this.comments.map((comment) => {
      if (comment.author === name || role === 'Admin') {
        comment.isAuthorOrAdmin = true; }

    });

  }
  editComment() {
    this.editMode = true;
    }


    closeComment() {
      this.showCreateComment = false;
    }
    finishEditComment(isUpdateSuccessful: boolean) {
      this.editMode = false;
    }

    updateComment(updatedComment: Comment) {
      this.comments.map(comment => {if (comment.id === updatedComment.id) {
        comment.content = updatedComment.content;
        comment.editMode = false;
      }}) ;
    }
    deleteComment(commentId: string) {
this.subscription = this.commentService.deleteCommentById(this.postId, commentId)
  .subscribe(
    res => {
      this.comments.map((comment, index) => {if (comment.id === commentId) {
        this.comments.splice(index, 1);
      }}) ;
    this.notificator.success(`Comment is deleted`);
        },
        err => {
            this.notificator.error(err.message);
          }
        );

    }
}
