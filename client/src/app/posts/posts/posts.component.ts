import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Post } from 'src/app/common/post';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from 'src/app/core/services/storage.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit, OnDestroy {
  public loggedUser: any;
  public user$: Subscription;
  public isFriended: boolean;
  @Input() posts: Post[];

  constructor(
    private readonly route: ActivatedRoute,
    private router: Router,
    private readonly storage: StorageService,
    private readonly http: HttpClient
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.posts = data.posts;
    });
    this.loggedUser = this.storage.get('username');
  }
  ngOnDestroy() {
  }

  showPost(id: string) {
    this.router.navigate(['/posts', id]);
  }
}
