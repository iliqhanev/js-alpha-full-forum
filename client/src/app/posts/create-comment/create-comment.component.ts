import { Post } from './../../common/post';
import { PostService } from './../../core/services/post.service';
import { CreatePost } from './../../common/create-post';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificatorService } from './../../../app/core/services/notificator.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { CommentService } from 'src/app/core/services/comment.service';


@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})

export class CreateCommentComponent implements OnInit {

  @Input() postId: string;
  @Output() addComment: EventEmitter<Object> = new EventEmitter<Object>();
  @Output() closeComment: EventEmitter<boolean> = new EventEmitter<boolean>();
  createCommentForm;

  constructor(
    private formBuilder: FormBuilder,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly comments: CommentService,
  ) { }


  ngOnInit() {

    this.createCommentForm = this.formBuilder.group({
      content: '',

    });
  }


  onSubmit(value) {

    this.comments.createCommentToPost(this.postId, value)
    .subscribe(
        res => {
        this.notificator.success(`Comment created`);
        this.addComment.emit(res);
            },
        err => {
              this.notificator.error(err.message);
            });
    }

    close(event) {
      event.stopPropagation();
      this.closeComment.emit(true);

    }
  }
