import { StorageService } from './../../../app/core/services/storage.service';
import { CommentService } from 'src/app/core/services/comment.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from './../../../app/common/post';
import { Subscription, of } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PostService } from './../../../app/core/services/post.service';
import { catchError } from 'rxjs/operators';
import { NotificatorService } from './../../../app/core/services/notificator.service';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.css']
})
export class ShowPostComponent implements OnInit {

  @Input() post: Post;
  @Input() comments: Comment[];
  createCommentToPostForm;
  isAuthorOrAdmin = false;
  updatePostForm: FormGroup;
  subscription: Subscription;
  deleteSubscription: Subscription;
  showCreateComment = false;
  editMode = false;

  constructor(
    private formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly commentService: CommentService,
    private readonly storage: StorageService,
    private readonly postService: PostService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    const name = this.storage.get('username');
    const role = this.storage.get('role');

    this.route.data.subscribe(
      data => {
        this.post = data.post;
        this.comments = data.comments;
        if (this.post.author === name || role === 'Admin') {
          this.isAuthorOrAdmin = true;
        }
      },
    );

    this.updatePostForm = this.formBuilder.group({
      title: '',
      content: ''
    });
  }

  addNewComment(newComment) {
    this.showCreateComment = false;
    newComment.isAuthorOrAdmin = true;
    this.comments.push(newComment);
  }

  closeComment() {
    this.showCreateComment = false;
  }


  deletePost(id: string) {
    this.deleteSubscription = this.postService.deletePostById(this.post.id)
      .pipe(
        catchError(err => {
          this.notificator.error(err.message);
          return of([]);
        })
      ).subscribe(
        res => {
          this.notificator.success(`Post is deleted`);
          this.router.navigate(['posts']);
        });
  }
  // TODO AutorOrAdminGuard =>

  editPost() {
    this.editMode = true;
  }
  likePost(id: string) { }
  finishEditPost(isSuccessful: boolean) {
    this.editMode = false;
  }


  dislikePost(id: string) { }

  onSubmit() {
    this.editMode = false;
    this.subscription = this.postService.updatePostById(this.post.id, this.post)
      .subscribe(
        res => {
          this.notificator.success(`Post is updated`);
        },
        err => {
          console.log();
          this.notificator.error(err.error.message);
        });
  }
  close(event) {
    this.editMode = false;
  }
}
