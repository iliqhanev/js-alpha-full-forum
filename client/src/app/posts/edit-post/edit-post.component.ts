import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Post } from 'src/app/common/post';
import { catchError } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators, Validator } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from 'src/app/core/services/post.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { title } from 'process';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  updatePostForm: FormGroup;
  subscription: Subscription;
  @Input() post: Post;
  @Output() notifyParent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() updatedPost: EventEmitter<Post | {}> = new EventEmitter<Post | {}>();

  constructor(private formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly postService: PostService, ) { }
  ngOnInit() {
    this.updatePostForm = this.formBuilder.group({
      title: '',
      content: ''
    });
    // [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],

    this.updatePostForm.controls['title'].setValue(this.post.title);
    this.updatePostForm.controls['content'].setValue(this.post.content);
  }

  onSubmit() {
    this.subscription = this.postService.updatePostById(this.post.id, this.updatePostForm.value)
      .pipe(
        catchError(err => {
          if (err) {
            this.notificator.error(err.message);
            this.notifyParent.emit(false);
            return of({});
          }
        })
      ).subscribe(
        res => {
          this.notificator.success(`Post is updated`);
          this.notifyParent.emit(true);
          this.updatedPost.emit(res);
        });
  }

  close(event) {
    event.stopPropagation();
    this.router.navigate(['posts']);

  }


  // close() {
  //   // this.editMode = false;
  // }
}
