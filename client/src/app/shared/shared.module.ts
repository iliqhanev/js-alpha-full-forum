import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  MatToolbarModule,
  MatToolbar,
  MatButtonModule,
  MatButton,
  MatSidenavModule,
  MatSidenav,
  MatSidenavContent,
  MatSidenavContainer,
  MatFormFieldModule,
  MatFormField,
  MatInputModule,
  MatInput,
  MatNavList,
  MatListModule,
  MatCardModule,
  MatCard,
  MatCardTitle,
  MatCardHeader,
  MatCardSubtitle,
  MatCardContent
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SettingsComponent } from '../components/settings/settings.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { UserSettingsComponent } from '../components/user-settings/user-settings.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    SettingsComponent,
    UserSettingsComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatTooltipModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    AngularFontAwesomeModule
  ],
  exports: [
    MatToolbar,
    MatTooltipModule,
    MatButton,
    MatSidenav,
    MatSidenavContent,
    MatSidenavContainer,
    MatFormField,
    MatInput,
    MatNavList,
    MatCard,
    MatCardTitle,
    MatCardHeader,
    MatCardSubtitle,
    MatCardContent,
    CommonModule,
    SettingsComponent,
    AngularFontAwesomeModule,
    UserSettingsComponent,
    MatIconModule
  ]
})
export class SharedModule { }

