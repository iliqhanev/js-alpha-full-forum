import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { PostsComponent } from './posts/posts/posts.component';
import { PostsResolverService } from './posts/services/posts-resolver.service';
import { CreatePostComponent } from './posts/create-post/create-post.component';
import { ShowPostComponent } from './posts/show-post/show-post.component';
import { ShowPostResolverService } from './posts/services/show-post-resolver.service';
import { CommentsResolverService } from './posts/services/comments-resolver.service';
import { ProfileComponent } from './components/profile/profile.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'posts', component: PostsComponent, canActivate: [AuthGuard], resolve: { posts: PostsResolverService } },
  {
    path: 'posts/:postId', component: ShowPostComponent, canActivate: [AuthGuard],
    resolve: { post: ShowPostResolverService, comments: CommentsResolverService },
  },
  { path: 'create-post', component: CreatePostComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
