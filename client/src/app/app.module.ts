import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './auth/token-interceptor.service';
import { PostsModule } from './posts/posts.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MAT_LABEL_GLOBAL_OPTIONS, } from '@angular/material';
import { ProfileComponent } from './components/profile/profile.component';
import { FooterComponent } from './components/footer/footer.component';
import { BoldElementDirective } from './sared/bold-element.directive';
import { ColorElementDirective } from './sared/color-element.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    ProfileComponent,
    FooterComponent,
    BoldElementDirective,
    ColorElementDirective
  ],
  imports: [
    ReactiveFormsModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    PostsModule,
    FormsModule,
    BrowserModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: MAT_LABEL_GLOBAL_OPTIONS,
      useValue: { float: 'always' },
    }
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
})
export class AppModule { }
