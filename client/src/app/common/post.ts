export interface Post {

  id: string;
  author: string;
  title: string;
  content: string;
  created: Date;
  postLikes?: [];
  postDislikes?: [];
 }
