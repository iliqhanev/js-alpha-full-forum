export interface Comment {
  id: string;
  author: string;
  created: Date;
  updated: Date;
  content: string;
  commentLikes: any;
  commentDislikes: any;
  isAuthorOrAdmin?: boolean;
  editMode?: boolean;
}
