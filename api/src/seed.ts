import "reflect-metadata";
import { createConnection, MongoEntityManager } from "typeorm";
import { User } from "./data/entities/user";
import { PostEntity } from "./data/entities/post";
import { Comment } from "./data/entities/comment";
import { PostLikes } from "./data/entities/post-like";
import { CommentLikes } from "./data/entities/comment-like";

const main = async () => {

  const connection = await createConnection();
  const manager = connection.manager;

  const userReposirory = manager.getRepository(User);
  const postReposirory = manager.getRepository(PostEntity);
  const commentRepository = manager.getRepository(Comment);
  const postLikeRepository = manager.getRepository(PostLikes);
  const commentLikeRepository = manager.getRepository(CommentLikes);

  // users
  const peter = new User();
  peter.name = 'Peter';
  peter.email = 'ppetrov@mail.bg';
  peter.dateCreated = new Date();
  peter.password = 'peter123';
  peter.access = 'user';
  peter.isDeleted = false;
  peter.isBanned = false;
  await userReposirory.save(peter);

  const joro = new User();
  joro.name = 'Joro';
  joro.email = 'jorosmail@gmail.com';
  joro.dateCreated = new Date();
  joro.password = 'joro123';
  joro.access = 'user';
  joro.isDeleted = false;
  joro.isBanned = false;
  await userReposirory.save(joro);

  const johny = new User();
  johny.name = 'Johny';
  johny.email = 'johnyboy@gmail.com';
  johny.dateCreated = new Date();
  johny.password = 'johny123';
  johny.access = 'user';
  johny.isDeleted = false;
  johny.isBanned = false;
  await userReposirory.save(johny);

  const yana = new User();
  yana.name = 'Yana';
  yana.email = 'beauty@abv.bg';
  yana.dateCreated = new Date();
  yana.password = 'yana123';
  yana.access = 'user';
  yana.isDeleted = false;
  yana.isBanned = false;
  await userReposirory.save(yana);

  const pipi = new User();
  pipi.name = 'Pipi';
  pipi.email = 'pipilota@mail.bg';
  pipi.dateCreated = new Date();
  pipi.password = 'pipi123';
  pipi.access = 'user';
  pipi.isDeleted = false;
  pipi.isBanned = false;
  await userReposirory.save(pipi);

  // posts
  const pipiPost = new PostEntity();
  pipiPost.author = Promise.resolve(pipi);
  pipiPost.title = 'Hello!';
  pipiPost.content = 'My name is Pipilota.';
  pipiPost.isLocked = false;
  pipiPost.isDeleted = false;
  await postReposirory.save(pipiPost);

  const yanaPost = new PostEntity();
  yanaPost.author = Promise.resolve(yana);
  yanaPost.title = 'Hi!';
  yanaPost.content = 'I am Yana. Age 18';
  yanaPost.isLocked = false;
  yanaPost.isDeleted = false;
  await postReposirory.save(yanaPost);

  const joroPost = new PostEntity();
  joroPost.author = Promise.resolve(joro);
  joroPost.title = 'Seed';
  joroPost.content = 'That is so booooring...';
  joroPost.isLocked = false;
  joroPost.isDeleted = false;
  await postReposirory.save(joroPost);

  const joroPost2 = new PostEntity();
  joroPost.author = Promise.resolve(joro);
  joroPost.title = 'Yes';
  joroPost.content = 'Ooooo Yeeeee...';
  joroPost.isLocked = false;
  joroPost.isDeleted = false;
  await postReposirory.save(joroPost2);

  // comments
  const johnyComment = new Comment();
  johnyComment.post = Promise.resolve(joroPost);
  johnyComment.author = Promise.resolve(johny);
  johnyComment.title = 'Sory dude';
  johnyComment.content = 'No pain no gain';
  johnyComment.isLocked = false;
  johnyComment.isDeleted = false;
  await commentRepository.save(johnyComment);

  const pipiComment = new Comment();
  pipiComment.post = Promise.resolve(joroPost);
  pipiComment.author = Promise.resolve(pipi);
  pipiComment.title = 'Take it easy';
  pipiComment.content = 'Get a Beer';
  pipiComment.isLocked = false;
  pipiComment.isDeleted = false;
  await commentRepository.save(pipiComment);

  const johnyComment2 = new Comment();
  johnyComment2.post = Promise.resolve(yanaPost);
  johnyComment2.author = Promise.resolve(johny);
  johnyComment2.title = 'lol';
  johnyComment2.content = `I'm afraid this is not the right forum for you`;
  johnyComment2.isLocked = false;
  johnyComment2.isDeleted = false;
  await commentRepository.save(johnyComment2);

  const yanaComment = new Comment();
  yanaComment.post = Promise.resolve(yanaPost);
  yanaComment.author = Promise.resolve(yana);
  yanaComment.title = 'no';
  yanaComment.content = `I'm sure this is the right forum`;
  yanaComment.isLocked = false;
  yanaComment.isDeleted = false;
  await commentRepository.save(yanaComment);

  const johnyComment3 = new Comment();
  johnyComment3.post = Promise.resolve(yanaPost);
  johnyComment3.author = Promise.resolve(johny);
  johnyComment3.title = 'ye ye';
  johnyComment3.content = `We will see...`;
  johnyComment3.isLocked = false;
  johnyComment3.isDeleted = false;

  // likes
  const like = new PostLikes();
  like.author = Promise.resolve(yana);
  like.vote = true;
  like.post = Promise.resolve(pipiPost);
  await postLikeRepository.save(like);

  const like1 = new PostLikes();
  like1.author = Promise.resolve(joro);
  like1.vote = true;
  like1.post = Promise.resolve(pipiPost);
  await postLikeRepository.save(like1);

  const like2 = new PostLikes();
  like2.author = Promise.resolve(joro);
  like2.vote = true;
  like2.post = Promise.resolve(yanaPost);
  await postLikeRepository.save(like2);

  const like3 = new CommentLikes();
  like3.author = Promise.resolve(joro);
  like3.vote = false;
  like3.comment = Promise.resolve(johnyComment2);
  await commentLikeRepository.save(like3);

  const like4 = new CommentLikes();
  like4.author = Promise.resolve(yana);
  like4.vote = false;
  like4.comment = Promise.resolve(johnyComment2);
  await commentLikeRepository.save(like4);

  const like5 = new CommentLikes();
  like5.author = Promise.resolve(yana);
  like5.vote = true;
  like5.comment = Promise.resolve(pipiComment);
  await commentLikeRepository.save(like5);

  const like6 = new CommentLikes();
  like6.author = Promise.resolve(joro);
  like6.vote = true;
  like6.comment = Promise.resolve(pipiComment);
  await commentLikeRepository.save(like6);

  const like7 = new CommentLikes();
  like7.author = Promise.resolve(johny);
  like7.vote = false;
  like7.comment = Promise.resolve(pipiComment);
  await commentLikeRepository.save(like7);

  // followers
  joro.followers = Promise.resolve([pipi, yana, johny]);
  const updateJoro = await userReposirory.save([joro]);

  pipi.followers = Promise.resolve([yana, johny]);
  const updatePipi = await userReposirory.save([pipi]);

  yana.followers = Promise.resolve([joro]);
  const updateYana = await userReposirory.save([yana]);

  connection.close();
};

// tslint:disable-next-line:no-console
main().catch(console.error);

// ***************************************************************
// //creating users
// const peter = await userReposirory.findOne({
//   where: { name: 'Peter' }
// });
// if (!peter) {
//   const user = new User();
//   user.name = 'Peter';
//   user.email = 'ppetrov@mail.bg';
//   user.dateCreated = new Date();
//   user.password = 'peter123';
//   user.access = 'user';
//   user.isDeleted = false;
//   user.isBanned = false;
//   await userReposirory.save(user);
//   console.log(`User Peter was createt`);
// } else {
//   console.log(`The name Peter already exist in DB`);
// }
//
// const joro = await userReposirory.findOne({
//   where: { name: 'Joro' }
// });
// if (!joro) {
//   const user = new User();
//   user.name = 'Joro';
//   user.email = 'jorosmail@gmail.com';
//   user.dateCreated = new Date();
//   user.password = 'joro123';
//   user.access = 'user';
//   user.isDeleted = false;
//   user.isBanned = false;
//   await userReposirory.save(user);
//   console.log(`User Joro was createt`);
// } else {
//   console.log(`The name Joro already exist in DB`);
// }
//
// const johny = await userReposirory.findOne({
//   where: { name: 'Johny' }
// });
// if (!johny) {
//   const user = new User();
//   user.name = 'Johny';
//   user.email = 'johnyboy@gmail.com';
//   user.dateCreated = new Date();
//   user.password = 'johny123';
//   user.access = 'user';
//   user.isDeleted = false;
//   user.isBanned = false;
//   await userReposirory.save(user);
//   console.log(`User johny was createt`);
// } else {
//   console.log(`The name johny already exist in DB`);
// }
//
// const yana = await userReposirory.findOne({
//   where: { name: 'Yana' }
// });
// if (!yana) {
//   const user = new User();
//   user.name = 'Yana';
//   user.email = 'beauty@abv.bg';
//   user.dateCreated = new Date();
//   user.password = 'yana123';
//   user.access = 'user';
//   user.isDeleted = false;
//   user.isBanned = false;
//   await userReposirory.save(user);
//   console.log(`User Yana was createt`);
// } else {
//   console.log(`The name Yana already exist in DB`);
// }

// const pipi = await userReposirory.findOne({
//   where: { name: 'Pipi' }
// });
// if (!pipi) {
//   const user = new User();
//   user.name = 'Pipi';
//   user.email = 'pipilota@mail.bg';
//   user.dateCreated = new Date();
//   user.password = 'pipi123';
//   user.access = 'user';
//   user.isDeleted = false;
//   user.isBanned = false;
//   await userReposirory.save(user);
//   console.log(`User Pipi was createt`);
// } else {
//   console.log(`The name Pipi already exist in DB`);
// }

// //creating posts
// const pipiPost = await postReposirory.findOne({
//   where: { title: 'Hello!' }
// });
// if (!pipiPost) {
//   const post = new Post();
//   post.author = Promise.resolve(pipi);
//   post.title = 'Hello!';
//   post.content = 'My name is Pipilota.';
//   post.isLocked = false;
//   post.isDeleted = false;
//   await postReposirory.save(post);
//   console.log(`The post was createt`);
// } else {
//   console.log(`The post already exist in DB`);
// }

// const yanaPost = await postReposirory.findOne({
//   where: { title: 'Hi!' }
// });
// if (!yanaPost) {
//   const post = new Post();
//   post.author = Promise.resolve(yana);
//   post.title = 'Hi!';
//   post.content = 'I am Yana. Age 18';
//   post.isLocked = false;
//   post.isDeleted = false;
//   await postReposirory.save(post);
//   console.log(`The post was createt`);
// } else {
//   console.log(`The post already exist in DB`);
// }

// const joroPost = await postReposirory.findOne({
//   where: { title: 'Seed' }
// });
// if (!joroPost) {
//   const post = new Post();
//   post.author = Promise.resolve(joro);
//   post.title = 'Seed';
//   post.content = 'That is so booooring...';
//   post.isLocked = false;
//   post.isDeleted = false;
//   await postReposirory.save(post);
//   console.log(`The post was createt`);
// } else {
//   console.log(`The post already exist in DB`);
// }

// //creating comments
// const johnyComment = await commentRepository.findOne({
//   where: { title: 'Sory dude' }
// });
// if (!johnyComment) {
//   const comment = new Comment();
//   comment.post = Promise.resolve(joroPost);
//   comment.author = Promise.resolve(johny);
//   comment.title = 'Sory dude';
//   comment.content = 'No pain no gain';
//   comment.isLocked = false;
//   comment.isDeleted = false;
//   await commentRepository.save(comment);
//   console.log(`The comment was createt`);
// } else {
//   console.log(`The comment already exist in DB`);
// }

// const pipiComment = await commentRepository.findOne({
//   where: { title: 'Take it easy' }
// });
// if (!pipiComment) {
//   const comment = new Comment();
//   comment.post = Promise.resolve(joroPost);
//   comment.author = Promise.resolve(pipi);
//   comment.title = 'Take it easy';
//   comment.content = 'Get a Beer';
//   comment.isLocked = false;
//   comment.isDeleted = false;
//   await commentRepository.save(comment);
//   console.log(`The comment was createt`);
// } else {
//   console.log(`The comment already exist in DB`);
// }

// const johnyComment2 = await commentRepository.findOne({
//   where: { title: 'lol' }
// });
// if (!johnyComment2) {
//   const comment = new Comment();
//   comment.post = Promise.resolve(yanaPost);
//   comment.author = Promise.resolve(johny);
//   comment.title = 'lol';
//   comment.content = `I'm afraid this is not the right forum for you`;
//   comment.isLocked = false;
//   comment.isDeleted = false;
//   await commentRepository.save(comment);
//   console.log(`The comment was createt`);
// } else {
//   console.log(`The comment already exist in DB`);
// }

// const yanaComment = await commentRepository.findOne({
//   where: { title: 'no' }
// });
// if (!yanaComment) {
//   const comment = new Comment();
//   comment.post = Promise.resolve(yanaPost);
//   comment.author = Promise.resolve(yana);
//   comment.title = 'no';
//   comment.content = `I'm sure this is the right forum`;
//   comment.isLocked = false;
//   comment.isDeleted = false;
//   await commentRepository.save(comment);
//   console.log(`The comment was createt`);
// } else {
//   console.log(`The comment already exist in DB`);
// }

// const johnyComment3 = await commentRepository.findOne({
//   where: { title: 'ye ye' }
// });
// if (!johnyComment3) {
//   const comment = new Comment();
//   comment.post = Promise.resolve(yanaPost);
//   comment.author = Promise.resolve(johny);
//   comment.title = 'ye ye';
//   comment.content = `We will see...`;
//   comment.isLocked = false;
//   comment.isDeleted = false;
//   await commentRepository.save(comment);
//   console.log(`The comment was createt`);
// } else {
//   console.log(`The comment already exist in DB`);
// }

  // //   TypeError: Cannot set property 'followers' of undefined
  // //   at main (D:\Desktop\js-alpha-forum\src\seed.ts:246:17)
  // //   at process._tickCallback (internal/process/next_tick.js:68:7)
  // //   (node:3164) [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues.
  // //   Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.

  // // followers
  // joro.followers = Promise.resolve([pipi, yana, johny]);
  // await userReposirory.save([joro]);

  // pipi.followers = Promise.resolve([yana, johny]);
  // await userReposirory.save([pipi]);

  // yana.followers = Promise.resolve([joro]);
  // await userReposirory.save([yana]);
