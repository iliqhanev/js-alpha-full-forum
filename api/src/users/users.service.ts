import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { ReturnFriendsDTO } from '../models/return-friends.dto';

@Injectable()
export class UsersService {

    constructor(@InjectRepository(User) private readonly userRepo: Repository<User>) { }

    async findUser(userName: string): Promise<User> {
        return await this.userRepo.findOne({ where: { name: userName } });
    }

    async banUser(userName: string, bannedReason: string): Promise<object> {
        await this.userRepo.update({ name: userName }, { isBanned: true, bannedReason });
        const found = await this.findUser(userName);
        return {
            message: `The following user was banned!`,
            user: found,
        };
    }

    async isBanned(userName: string) {
        const user = await this.userRepo.findOne({ name: userName });

        if (user.isBanned === true) {
            return {
                message: `You have been banned!`,
                reason: user.bannedReason,
            };
        }

        return false;
    }

    async deleteUserAccount(userName: string) {
        await this.userRepo.update({ name: userName }, { isDeleted: true });
        const found = await this.findUser(userName);
        return {
            message: `The following user account was removed!`,
            account: found,
        };
    }

    async allFriends(user: User) {
        const filteredFriends: any = await this.userRepo.findOne({
            relations: ['following'],
            where: {
                id: user.id,
            },
        });

        const following = filteredFriends.__following__.filter(friend => friend.isUnfriended === false);

        return {
            yourFriends: plainToClass(ReturnFriendsDTO, following, { excludeExtraneousValues: true }),
        };
    }

    async addFriend(user: User, friendName: string): Promise<object> {
        const foundFriend = await this.findUser(friendName);
        const currentUser = await this.findUser(user.name);
        const friendsToReturn = plainToClass(ReturnFriendsDTO, foundFriend, { excludeExtraneousValues: true });

        const friends = await currentUser.following;
        friends.push(foundFriend);
        currentUser.following = Promise.resolve(friends);

        await this.userRepo.save(currentUser);
        await this.userRepo.update({ name: foundFriend.name }, {
            isUnfriended: false,
        });

        return {
            name: currentUser.name,
            email: currentUser.email,
            message: `You started to follow the following person`,
            following: friendsToReturn,
        };
    }

    async removeFriend(user: User, friendName: string): Promise<object> {
        const foundFriend = await this.findUser(friendName);
        const currentUser = await this.findUser(user.name);
        const friendsToReturn = plainToClass(ReturnFriendsDTO, foundFriend, { excludeExtraneousValues: true });

        const currentUserFollowing = await currentUser.following;
        const removable = currentUserFollowing.findIndex(following => following.name === foundFriend.name);

        await this.userRepo.update({ name: friendName }, {
            isUnfriended: true,
        });

        currentUserFollowing.splice(removable, 1);
        await this.userRepo.save(currentUserFollowing);

        return {
            name: currentUser.name,
            email: currentUser.email,
            message: `You no longer follow the following person`,
            removed: friendsToReturn,
        };
    }
}
