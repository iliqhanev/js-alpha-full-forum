import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { AuthModule } from '../auth/auth.module';
import { PostsService } from '../posts/posts.service';
import { AuthService } from '../auth/auth.service';

@Module({
  controllers: [UsersController],
  imports: [TypeOrmModule.forFeature([User]), AuthModule],
  providers: [UsersService, AuthService, PostsService],
  exports: [UsersService],
})
export class UsersModule { }
