import { Controller, Get, Param, UseGuards, Post, Delete, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/user-role.enum';
import { UsersService } from './users.service';
import { User } from '../common/decorators/user.decorator';
import { TokenGuard } from '../common/guards/token.guard';
import { FindUserGuard } from '../common/guards/find-user.guard';

@Controller('users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService) { }

    @Get()
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard)
    async showMyFriends(@User() user: any): Promise<object | string> {
        const friends = await this.usersService.allFriends(user);
        return friends;
    }

    @Post('friends/:friendName')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard, FindUserGuard)
    async addFriend(
        @Param('friendName') friendName: string,
        @User() user: any) {
        return await this.usersService.addFriend(user, friendName);
    }

    @Delete('friends/:friendName')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard, FindUserGuard)
    async removeFriend(
        @Param('friendName') friendName: string,
        @User() user: any): Promise<object> {
        return await this.usersService.removeFriend(user, friendName);
    }

    @Delete('ban/:friendName')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard, FindUserGuard)
    async banUser(
        @Param('friendName') friendName: string,
    ) {
        return await this.usersService.banUser(friendName, '');
    }

    @Delete('deleteuser/:friendName')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard, FindUserGuard)
    async deleteUser(
        @Param('friendName') friendName: string,
    ) {
        return await this.usersService.deleteUserAccount(friendName);
    }
}
