import { Module} from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';
import { Comment } from '../data/entities/comment';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentLikes } from '../data/entities/comment-like';
import { User } from '../data/entities/user';
import { AuthModule } from '../auth/auth.module';
import { PostEntity } from '../data/entities/post';
import { PostsModule } from '../posts/posts.module';

@Module({
  imports: [TypeOrmModule.forFeature([ Comment, CommentLikes, User, PostEntity]), AuthModule, PostsModule],
  controllers: [CommentsController],
  providers: [CommentsService],
  exports: [CommentsService],
})
export class CommentsModule {}
