import { Controller, Get, Post, Body, ValidationPipe, Req, UseGuards, Param, Put, BadRequestException, Delete, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UpdateCommentDTO, ReturnCommentDTO, CreateCommentDTO } from '../models';
import { PostsService } from '../posts/posts.service';
import { CommentsService } from './comments.service';
import { User } from '../common/decorators/user.decorator';
@Controller()
export class CommentsController {

    constructor(private readonly commentService: CommentsService, private readonly postsService: PostsService) { }

    @UseGuards(AuthGuard())
    @Get('posts/:postId/comments')
    async returnAllCommentsByPost(
        @Param('postId') postId: string): Promise<ReturnCommentDTO[]> {
        const found = await this.postsService.findPost(postId);
        if (!found) {
            throw new NotFoundException(`Post with '${postId}' does not exist!`);
        }

        return await this.commentService.allCommentsByPost(postId);
    }

    @UseGuards(AuthGuard())
    @Get('users/:userId/comments')
    async returnAllCommentsByUser(
        @Param('userId') userId: string,
        @Req() author: any): Promise<ReturnCommentDTO[]> {
        if (userId !== author.id && author.role !== 'Admin') {
            throw new UnauthorizedException('You has no right to read this comments');
        }
        return await this.commentService.allCommentsByUser(author);
    }

    @Post('comments/:postId/:commentId/vote')
    async likeComment(
        @Param('postId') postId: string,
        @Param('commentId') commentId: string,
        @User() voter: any) {
        const found = await this.postsService.findPost(postId);
        if (!found) { throw new NotFoundException(`Post with '${postId}' does not exist!`); }

        const foundComment = await this.commentService.fundComment(commentId);
        if (!foundComment) { throw new NotFoundException(`Comment with '${commentId}' does not exist!`); }

        return await this.commentService.likeComment(voter, voter.id, postId, commentId);
    }

    @UseGuards(AuthGuard())
    @Post('posts/:postId/comments')
    async createComment(
        @Body(new ValidationPipe({ transform: true, whitelist: true })) comment: CreateCommentDTO,
        @User() author: any,
        @Param('postId') postId: string): Promise<ReturnCommentDTO> {
        const found = await this.postsService.findPost(postId);
        if (!found) { throw new NotFoundException(`Post with '${postId}' does not exist!`); }

        return await this.commentService.createComment(comment, author, postId);
    }

    @UseGuards(AuthGuard())
    @Put('posts/:postId/comments/:commentId')
    async updateComment(
        @Body(new ValidationPipe({ transform: true, whitelist: true })) updateComment: UpdateCommentDTO,
        @Param('postId') postId: string,
        @Param('commentId') commentId: string,
        @User() user: any) {
        const foundPost = await this.postsService.findPost(postId);
        if (!foundPost) { throw new NotFoundException(`Post with '${postId}' does not exist!`); }

        const foundComment = await this.commentService.fundComment(commentId);
        if (!foundComment) { throw new NotFoundException(`Comment with '${commentId}' does not exist!`); }

        if (user.name !== foundComment.author && user.role !== 'Admin') {
            throw new UnauthorizedException('You has no right to update this comment');
        }

        return await this.commentService.updateComment(commentId, updateComment);
    }

    @UseGuards(AuthGuard())
    @Delete('posts/:postId/comments/:commentId')
    async deleteComment(
        @Param('postId') postId: string,
        @Param('commentId') commentId: string,
        @User() user: any) {

        const foundPost = await this.postsService.findPost(postId);
        if (!foundPost) { throw new NotFoundException(`Post with '${postId}' does not exist!`); }

        const foundComment = await this.commentService.fundComment(commentId);
        if (!foundComment) { throw new NotFoundException(`Comment with '${commentId}' does not exist!`); }

        if (user.name !== foundComment.author && user.role !== 'Admin') {
            throw new UnauthorizedException('You has no right to update this comment');
        }
        return await this.commentService.deleteComment(commentId);
    }
}
