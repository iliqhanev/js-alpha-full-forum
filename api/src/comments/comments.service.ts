
import { Comment } from './../data/entities/comment';
import { Repository } from 'typeorm';
import { Injectable, BadRequestException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { InjectRepository } from '@nestjs/typeorm';
import { PostEntity } from '../data/entities/post';
import { User } from '../data/entities/user';
import { UpdateCommentDTO, CreateCommentDTO, ReturnCommentDTO } from './../models';
import { CommentLikes } from '../data/entities/comment-like';
import { PostLikes } from '../data/entities/post-like';

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(Comment) private readonly commentRepo: Repository<Comment>,
        @InjectRepository(CommentLikes) private readonly commentLikesRepo: Repository<CommentLikes>,
        @InjectRepository(PostEntity) private readonly postRepo: Repository<PostEntity>,
    ) { }

    async fundComment(commentId: string): Promise<ReturnCommentDTO> {
        const foundComment: any = await this.commentRepo.findOne({
            relations: ['author'],
            where: {
                id: commentId,
                isDeleted: false,
            },
        });

        const commentToReturn = plainToClass(ReturnCommentDTO, {
            ...foundComment,
            author: foundComment.__author__.name,
        }, {
                excludeExtraneousValues: true,
            });

        return commentToReturn;
    }

    async createComment(comment: CreateCommentDTO, author: User, postId: string): Promise<ReturnCommentDTO> {
        let foundPost: PostEntity;
        try {
            foundPost = await this.postRepo.findOne({ id: postId });
        } catch (err) {
            throw new BadRequestException(`Post is not found`);
        }
        const commentForCreation = new Comment();
        commentForCreation.content = comment.content;
        commentForCreation.author = Promise.resolve(author);
        commentForCreation.post = Promise.resolve(foundPost);
        const savedComment: Comment = await this.commentRepo.save(commentForCreation);
        const savedAuthor = await savedComment.author;
        const commentToReturn = plainToClass(ReturnCommentDTO, {
            ...savedComment,
            author: savedAuthor.name,
        }, {
                excludeExtraneousValues: true,
            });

        return commentToReturn;
    }

    async updateComment(commentId: string, updateComment: UpdateCommentDTO): Promise<ReturnCommentDTO> {
        await this.commentRepo.update({ id: commentId }, updateComment);

        const foundComment: any = await this.commentRepo.findOne({
            relations: ['author'],
            where: {
                id: commentId,
                isDeleted: false,
            },
        });

        const commentToReturn = plainToClass(ReturnCommentDTO, {
            ...foundComment,
            author: foundComment.__author__.name,
        }, {
                excludeExtraneousValues: true,
            });

        return commentToReturn;
    }

    async deleteComment(commentId: string) {
        await this.commentRepo.update({ id: commentId }, { isDeleted: true });

        const foundComment: any = await this.commentRepo.findOne({
            relations: ['author'],
            where: {
                id: commentId,
            },
        });

        const commentToReturn = plainToClass(ReturnCommentDTO, {
            ...foundComment,
            author: foundComment.__author__.name,
        }, {
                excludeExtraneousValues: true,
            });
        return {
            message: `The following comment was sucessfully removed!`,
            comment: commentToReturn,
        };
    }

    async allCommentsByPost(postId: string): Promise<ReturnCommentDTO[]> {
        const allComments = await this.commentRepo.find({
            relations: ['author'],
            where: {
                post: { id: postId },
                isDeleted: false,
            },
        });

        const likes: any = await this.commentLikesRepo.find({
            relations: ['author', 'comment'],
            where: {
                vote: 1,
            },
        });

        const disLikes: any = await this.commentLikesRepo.find({
            relations: ['author', 'comment'],
            where: {
                vote: 0,
            },
        });

        const returnComments = allComments.map((comment: any) => plainToClass(ReturnCommentDTO, {
            ...comment,
            commentLikes: likes
                .filter((like: any) => like.__comment__.id === comment.id)
                .map((author: any) => author.__author__.name),
            commentDislikes: disLikes
                .filter((like: any) => like.__comment__.id === comment.id)
                .map((author: any) => author.__author__.name),
            author: comment.__author__.name,
        }, {
                excludeExtraneousValues: true,
            }));

        return returnComments;
    }

    async likeComment(user: User, userID: string, commentId: string, vote: string) {

        const checkLikes: CommentLikes[] =
            await this.commentLikesRepo.find({ relations: ["author", "comment"] });
        const likes: CommentLikes[] = checkLikes
            .filter((obj: any) => obj.__author__.id === userID)
            .filter((obj: any) => obj.__comment__.id === commentId);

        const foundComment = await this.commentRepo.findOne({
            where: {
                id: commentId,
            },
        });

        if (likes[0]) {
            if (!likes[0].vote && vote === 'false') {
                likes[0].vote = false;
            } else if (!likes[0].vote && vote === 'true') {
                likes[0].vote = true;
            } else {
                likes[0].vote = vote === 'true';
            }

            await this.commentLikesRepo.save(likes[0]);

            return likes[0];
        }

        const like = new CommentLikes();
        like.author = Promise.resolve(user);
        like.vote = vote === 'true';
        like.comment = Promise.resolve(foundComment);
        const newPostLike = await this.commentLikesRepo.save(like);

        return newPostLike;
    }

    async allCommentsByUser(author: User): Promise<ReturnCommentDTO[]> {
        const allComents = await this.commentRepo.find({
            relations: ['author'],
            where: {
                author: { id: author.id },
                isDeleted: false,
            },
        });
        const returnComments = allComents.map((comment: any) => plainToClass(ReturnCommentDTO, {
            ...comment,
            author: comment.__author__.name,
        }, {
                excludeExtraneousValues: true,
            }));

        return returnComments;
    }

}
