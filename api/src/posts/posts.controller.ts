import { Controller, Get, Post, Body, ValidationPipe, UseGuards, Param, Put, Delete } from '@nestjs/common';
import { PostsService } from './posts.service';
import { AuthGuard } from '@nestjs/passport';
import { UpdatePostDTO, ReturnPostDTO, CreatePostDTO } from '../models/index';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { Roles } from '../common/decorators/roles.decorator';
import { User } from '../common/decorators/user.decorator';
import { Token } from '../common/decorators/token.decorator';
import { IsPostLockedGuard } from '../common/guards/is-post-locked.guard';
import { TokenGuard } from '../common/guards/token.guard';
import { FindPostGuard } from '../common/guards/find-post.guard';
import { AuthorAdminGuard } from '../common/guards/access-level.guard';
@Controller()
export class PostsController {

    constructor(
        private readonly postsService: PostsService,
    ) { }

    @Get('posts')
    @UseGuards(AuthGuard())
    async returnallPosts(): Promise<ReturnPostDTO[]> {
        return await this.postsService.allPosts();
    }

    @Get('posts/:postId')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard())
    async readIndividualPost(
        @Param('postId') postId: string): Promise<ReturnPostDTO> {
        return await this.postsService.readIndividualPost(postId);
    }

    @Put('posts/:id')
    // @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard())
    async updatePost(
        @Body(new ValidationPipe({ transform: true, whitelist: true })) updatePost: UpdatePostDTO,
        @Param('id') postId: string) {
        return await this.postsService.updatePost(postId, updatePost);
    }

    @Delete('posts/:id')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(),
    )
    async deletePost(
        @Param('id') postId: string) {
        return await this.postsService.deletePost(postId);
    }

    @Post('posts')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard)
    async createPost(
        @Body(new ValidationPipe({ transform: true, whitelist: true })) post: CreatePostDTO,
        @User() author: any): Promise<ReturnPostDTO> {
        return await this.postsService.createPost(post, author);
    }

    @Post('posts/flagPost/:postId')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard, FindPostGuard, TokenGuard)
    async flagPost(
        @Param('postId') postId: string) {
        return await this.postsService.flagPost(postId);
    }

    @Post('posts/vote/:postId/:voteValue')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard())
    async likePost(
        @User() user: any,
        @Param('postId') postId: string,
        @Param('voteValue') voteValue: string) {
        return await this.postsService.likePost(user, user.id, postId, voteValue);
    }

    @Delete('posts/lock/:postId')
    @Roles(UserRole.Admin)
    @UseGuards(AuthGuard(), RolesGuard, TokenGuard)
    async lockPost(
        @Param('postId') postId: string) {
        return await this.postsService.lockPost(postId);
    }
}
