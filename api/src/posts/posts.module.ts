import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { PostEntity } from '../data/entities/post';
import { UsersService } from '../users/users.service';
import { AuthModule } from '../auth/auth.module';
import { PostLikes } from '../data/entities/post-like';
import { AuthService } from '../auth/auth.service';

@Module({
    imports: [TypeOrmModule.forFeature([PostEntity, PostLikes]), AuthModule],
    providers: [PostsService, UsersService, AuthService],
    controllers: [PostsController],
    exports: [PostsService],
})
export class PostsModule { }
