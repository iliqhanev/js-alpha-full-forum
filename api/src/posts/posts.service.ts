import { Repository } from 'typeorm';
import { Injectable, BadRequestException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { InjectRepository } from '@nestjs/typeorm';
import { PostEntity } from '../data/entities/post';
import { User } from '../data/entities/user';
import { UpdatePostDTO, CreatePostDTO, ReturnPostDTO } from '../models/index';
import { PostLikes } from '../data/entities/post-like';
import { UserRole } from '../common/enums/user-role.enum';
@Injectable()
export class PostsService {

    constructor(
        @InjectRepository(PostEntity) private readonly postRepo: Repository<PostEntity>,
        @InjectRepository(PostLikes) private readonly postLikesRepo: Repository<PostLikes>) { }

    async AuthorOrAccessLevel(user: User, postId: string) {
        const post: any = await this.postRepo.findOne({
            relations: ['author'],
            where: {
                id: postId,
            },
        });

        const isAuthor = user.id === post.__author__.id;
        const isAdmin = user.roles
            .map((role: any) => role.name)
            .includes(UserRole.Admin);

        if (isAuthor || isAdmin) {
            return true;
        }
        throw new BadRequestException(`You can not edit this post!`);
    }

    async allPosts(): Promise<ReturnPostDTO[]> {
        const allPosts = await this.postRepo.find({
            relations: ['author'],
            where: {
                isDeleted: false,
            },
        });

        const likes: any = await this.postLikesRepo.find({
            relations: ['author', 'post'],
            where: {
                vote: 1,
            },
        });

        const disLikes: any = await this.postLikesRepo.find({
            relations: ['author', 'post'],
            where: {
                vote: 0,
            },
        });

        return allPosts.map((post: any) => plainToClass(ReturnPostDTO, {
            ...post,
            postLikes: likes
                .filter((like: any) => like.__post__.id === post.id)
                .map((author: any) => author.__author__.name),
            postDislikes: disLikes
                .filter((like: any) => like.__post__.id === post.id)
                .map((author: any) => author.__author__.name),
            author: post.__author__.name,
        }, { excludeExtraneousValues: true }));
    }

    async readIndividualPost(postId: string): Promise<ReturnPostDTO> {
        const post: any = await this.postRepo.findOne({
            relations: ['author'],
            where: { id: postId },
        });
        const likes: any = await this.postLikesRepo.find({
            relations: ['author'],
            where: { post: postId, vote: 1 },
        });
        const dislikes: any = await this.postLikesRepo.find({
            relations: ['author'],
            where: { post: postId, vote: 0 },
        });
        const postLikes = likes.map(() => post.__author__.name);
        const postDislikes = dislikes.map(() => post.__author__.name);
        const author = post.__author__.name;

        return plainToClass(ReturnPostDTO, {
            ...post,
            postLikes,
            postDislikes,
            author,
        }, { excludeExtraneousValues: true });
    }

    async findPost(postId: string) {
        const found = await this.postRepo.findOne({ id: postId });
        if (!found) {
            throw new BadRequestException(`The post you're looking for is not found!`);
        }
        return found;
    }

    async lockPost(postId: string): Promise<object> {
        const post = await this.postRepo.findOne({ id: postId });
        const postToReturn = plainToClass(ReturnPostDTO, post, {
            excludeExtraneousValues: true,
        });

        if (!post.isLocked) {
            await this.postRepo.update({ id: postId }, { isLocked: true });
            return {
                message: `You have locked the following post from changes!`,
                postToReturn,
                isLocked: true,
            };
        }

        await this.postRepo.update({ id: postId }, { isLocked: false });
        return {
            message: `You have unlocked the following post from for!`,
            postToReturn,
            isLocked: false,
        };
    }

    async isLocked(postId: string, user?: User) {
        const post = await this.findPost(postId);
        let isAdmin = false;
        if (user) {
            const admin = user.roles
                .map((role: any) => role.name)
                .includes(UserRole.Admin);
            isAdmin = admin;
        }
        if (isAdmin) {
            return true;
        } else {
            throw new BadRequestException(`This post is locked for changes!`);
        }
    }

    async createPost(post: CreatePostDTO, author: User): Promise<ReturnPostDTO> {
        const newPost = new PostEntity();
        newPost.content = post.content;
        newPost.title = post.title;
        newPost.author = Promise.resolve(author);
        newPost.isDeleted = false;
        newPost.isFlagged = false;
        newPost.isLocked = false;

        const savedPost = await this.postRepo.save(newPost);

        return plainToClass(ReturnPostDTO, { ...savedPost, author: author.name }, {
            excludeExtraneousValues: true,
        });
    }

    async updatePost(postId: string, updatePost: UpdatePostDTO): Promise<ReturnPostDTO> {
        await this.postRepo.update({ id: postId }, updatePost);
        const post: any = await this.postRepo.findOne({
            relations: ['author'],
            where: {
                id: postId,
            },
        });

        return plainToClass(ReturnPostDTO, {
            ...post,
            author: post.__author__.name,
        }, { excludeExtraneousValues: true });
    }

    async deletePost(postId: string) {
        await this.postRepo.update({ id: postId }, {
            isDeleted: true,
        });

        const post = await this.findPost(postId);

        return plainToClass(ReturnPostDTO, post, {
            excludeExtraneousValues: true,
        });
    }

    async likePost(user: User, userID: string, postID: string, vote: string) {

        const checkLikes: PostLikes[] =
            await this.postLikesRepo.find({ relations: ["author", "post"] });

        const likes: PostLikes[] = checkLikes
            .filter((obj: any) => obj.__author__.id === userID)
            .filter((obj: any) => obj.__post__.id === postID);

        if (likes[0]) {
            if (!likes[0].vote && vote === 'false') {
                likes[0].vote = false;
            } else if (!likes[0].vote && vote === 'true') {
                likes[0].vote = true;
            } else {
                likes[0].vote = vote === 'true';
            }

            await this.postLikesRepo.save(likes[0]);

            return likes[0];
        }

        const foundPost = await this.postRepo.findOne({
            where: {
                id: postID,
            },
        });

        const like = new PostLikes();
        like.author = Promise.resolve(user);
        like.vote = vote === 'true';
        like.post = Promise.resolve(foundPost);
        const newPostLike = await this.postLikesRepo.save(like);

        return newPostLike;
    }

    async flagPost(postId: string) {
        await this.postRepo.update({ id: postId }, { isFlagged: true });
        const post: any = await this.postRepo.findOne({
            relations: ['author'],
            where: {
                id: postId,
            },
        });

        return {
            message: `The following post was flagged succesfully and notification was sent to the admin team!`,
            post: plainToClass(ReturnPostDTO, {
                ...post,
                author: post.__author__.name,
            }, { excludeExtraneousValues: true }),
        };
    }
}
