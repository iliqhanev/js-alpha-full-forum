import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { RegisterUserDTO, UserLoginDTO } from '../models';
import { request } from 'https';
import { Request } from 'express';

// TODO
describe('Posts Controller', () => {
  let controller: AuthController;
  const service: any = { };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [{
        provide: AuthService,
        useValue: service,
      }],
    }).compile();

      // isBlackListed: async () => ({ token: 'test' }) || undefined,  //???
      // isUserRegistered: async () => ({}) || undefined, //???
      // register: async () => ({}),
      // login: async () => ({}),
      // logout: async () => ({}),

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    service.login = async () => ({});
    expect(controller).toBeDefined();
  });

  it('register should be return string if service provide undefined', async () => {
    // Arrange
    const req: any = Promise.resolve({}); // ??? real request mock
    service.isBlackListed = async () => undefined;

    // Act
    const result = await controller.private(req);
    // Assert
    expect(result).toBe(`hi this is private!`);
  });

  it('login should be return `hi this is private!`', async () => {
    // Arrange
    const regLoginDTO: UserLoginDTO = {
      name: 'name',
      password: 'password',
      email: 'email',
    };
    // Act
    const result = await controller.login(regLoginDTO);
    // Assert
    expect(result).toEqual({});
  });

  it('logout should be return object ', async () => {
    // Arrange
    const request: any = ({});
    // Act
    const result = await controller.logout(request);
    // Assert
    expect(result).toEqual({
      message: `You have been logged out succesfully!`,
    });
  });

});

  // it('register should be return `hi this is private!`', async () => {
  //   // Arrange
  //   const regUserDTO: RegisterUserDTO = {
  //     name: 'name',
  //     password: 'password',
  //     email:  'email',
  //   };
  //   // Act
  //   const result = await controller.register(regUserDTO);
  //   // Assert
  //   expect(result).toBe(`hi this is private!`);
  // });
