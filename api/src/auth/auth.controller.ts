import { Controller, Post, Body, ValidationPipe, BadRequestException, All, Get, Delete, Req, UseGuards } from '@nestjs/common';
import { ReturnUserDTO } from '../models';
import { RegisterUserDTO } from '../models/register-user.dto';
import { AuthService } from './auth.service';
import { UserLoginDTO } from '../models/user-login.dto';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/user-role.enum';
import { UsersService } from '../users/users.service';
@Controller('auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
    ) { }

    @Post()
    async register(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: RegisterUserDTO): Promise<object> {

        const found = await this.authService.isUserRegistered(user.name);

        if (found) {
            throw new BadRequestException(`The name you're using is already taken!`);
        }

        return await this.authService.register(user);
    }

    @Post('session')
    async login(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserLoginDTO): Promise<object | boolean> {
        const found = await this.authService.isUserRegistered(user.name);

        if (!found) {
            throw new BadRequestException(`Wrong name, try again!`);
        }

        const isBanned = await this.usersService.isBanned(user.name);

        if (isBanned) {
            throw new BadRequestException(`${isBanned.message}, reason : '${isBanned.reason}'`);
        }

        const isDeleted = await this.authService.isAccountDeleted(user.name);
        if (isDeleted) {
            throw new BadRequestException(`This account has been deleted!`);
        }

        const pwCompareResult = await this.authService.login(user);

        if (!pwCompareResult) {
            throw new BadRequestException(`Passwords didnt match, try again!`);
        }

        return pwCompareResult;
    }

    @Delete('session')
    @Roles(UserRole.Admin, UserRole.Basic)
    @UseGuards(AuthGuard(), RolesGuard)
    async logout(@Req() request: Request): Promise<object> {
        const token = { token: request.headers.authorization };

        await this.authService.logout(token);

        return {
            message: `You have been logged out succesfully!`,
        };
    }

}
