import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { Repository } from 'typeorm';
import { RegisterUserDTO } from '../models/register-user.dto';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { ReturnUserDTO } from '../models/return-user.dto';
import { UserLoginDTO } from '../models/user-login.dto';
import { JwtService } from '@nestjs/jwt';
import { BlackListedTokens } from '../data/entities/black-list-tokens';
import { BlackListTokenDTO } from '../models/black-list-token.dto';
import { UserRole } from '../common/enums/user-role.enum';
import { Role } from '../data/entities/role';
@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepo: Repository<Role>,
        @InjectRepository(BlackListedTokens) private readonly blackListedTokensRepo: Repository<BlackListedTokens>,
        private readonly jwtService: JwtService,
    ) { }

    async isUserRegistered(name: string): Promise<User | undefined> {
        return await this.userRepo.findOne({
            where: {
                name,
            },
        });
    }

    async validateUserIfExist(payload: any) {
        return await this.userRepo.findOne({
            where: {
                name: payload.name,
            },
        });
    }

    async register(user: RegisterUserDTO): Promise<object> {
        const basicRole = await this.rolesRepo.findOne({
            where: {
                name: UserRole.Basic,
            },
        });
        const userToSave = await {
            ...user, password: await bcrypt.hash(user.password, 10), isBanned: false,
            bannedReason: 'notBanned', isUnfriended: false, isDeleted: false, roles: [basicRole],
        };
        const savedUser = await this.userRepo.save(userToSave);

        return {
            message: `The following user have been registered succesfully!`,
            user: plainToClass(ReturnUserDTO, savedUser, {
                excludeExtraneousValues: true,
            }),
        };
    }

    async login(user: UserLoginDTO): Promise<object | boolean> {
        const foundUser = await this.userRepo.findOne({ where: { name: user.name } });
        const result = await bcrypt.compare(user.password, foundUser.password);

        if (!result) {
            return false;
        }

        const userRoles = await foundUser.roles;
        const displayUserRoles = userRoles.map((role) => role.name);
        const token = await this.jwtService.sign({ ...user });

        return {
            message: `You've logged in successfully!`,
            yourAccessLevel: `${displayUserRoles}`,
            token,
        };
    }

    async logout(token: BlackListTokenDTO) {
        const tokenToSave = { ...token, createdOn: new Date() };

        return await this.blackListedTokensRepo.save(tokenToSave);
    }

    async isBlackListed(token: string) {
        const objectTokens: any = await this.blackListedTokensRepo.find();
        const isBlackListed = objectTokens
            .map(obj => obj.token)
            .includes(token);

        if (isBlackListed) {
            throw new BadRequestException(`You need to log in!`);
        }

        return false;
    }

    async isAccountDeleted(userName: string) {
        const found = await this.userRepo.findOne({ name: userName });
        return found.isDeleted === true;
    }
}
