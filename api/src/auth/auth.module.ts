import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { BlackListedTokens } from '../data/entities/black-list-tokens';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { config } from '../common/config';
import { Role } from '../data/entities/role';
import { PostEntity } from '../data/entities/post';
import { PostsService } from '../posts/posts.service';
import { PostLikes } from '../data/entities/post-like';
import { UsersService } from '../users/users.service';
import { Comment } from '../data/entities/comment';
@Module({
    imports: [
        TypeOrmModule.forFeature([User, BlackListedTokens, Role, PostEntity, PostLikes, Comment]),
        ConfigModule,
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secretOrPrivateKey: config.jwtSecret,
                type: configService.dbType as any,
                host: configService.dbHost,
                port: configService.dbPort,
                username: configService.dbUsername,
                password: configService.dbPassword,
                database: configService.dbName,
                entities: ['./src/data/entities/*.ts'],
                signOptions: {
                    expiresIn: config.expiresIn, // one hour
                },
            }),
        }),

    ],
    providers: [AuthService, JwtStrategy, PostsService, UsersService],
    controllers: [AuthController],
})
export class AuthModule { }
