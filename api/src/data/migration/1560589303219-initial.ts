import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1560589303219 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `black_listed_tokens` (`id` varchar(36) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `token` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `post_likes` (`id` varchar(36) NOT NULL, `vote` tinyint NOT NULL, `postId` varchar(36) NULL, `authorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL, `isUnfriended` tinyint NOT NULL, `isBanned` tinyint NOT NULL, `bannedReason` varchar(255) NOT NULL, UNIQUE INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` (`name`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), UNIQUE INDEX `IDX_450a05c0c4de5b75ac8d34835b` (`password`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `posts` (`id` varchar(36) NOT NULL, `title` varchar(50) NOT NULL, `content` varchar(500) NOT NULL, `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isLocked` tinyint NOT NULL, `isDeleted` tinyint NOT NULL, `isFlagged` tinyint NOT NULL, `authorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comments` (`id` varchar(36) NOT NULL, `content` varchar(500) NOT NULL, `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isLocked` tinyint NOT NULL DEFAULT 0, `isDeleted` tinyint NOT NULL DEFAULT 0, `isFlagged` tinyint NOT NULL DEFAULT 0, `postId` varchar(36) NULL, `authorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comment_likes` (`id` varchar(36) NOT NULL, `vote` tinyint NOT NULL, `authorId` varchar(36) NULL, `commentId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_roles_roles` (`usersId` varchar(36) NOT NULL, `rolesId` varchar(36) NOT NULL, INDEX `IDX_df951a64f09865171d2d7a502b` (`usersId`), INDEX `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`), PRIMARY KEY (`usersId`, `rolesId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_following_users` (`usersId_1` varchar(36) NOT NULL, `usersId_2` varchar(36) NOT NULL, INDEX `IDX_f257a3163914a2e398d7bfa800` (`usersId_1`), INDEX `IDX_4a5ef04f52de98c49916c8798e` (`usersId_2`), PRIMARY KEY (`usersId_1`, `usersId_2`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `post_likes` ADD CONSTRAINT `FK_6999d13aca25e33515210abaf16` FOREIGN KEY (`postId`) REFERENCES `posts`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post_likes` ADD CONSTRAINT `FK_13cc94053492d02340fe803ca43` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posts` ADD CONSTRAINT `FK_c5a322ad12a7bf95460c958e80e` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_e44ddaaa6d058cb4092f83ad61f` FOREIGN KEY (`postId`) REFERENCES `posts`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_4548cc4a409b8651ec75f70e280` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_likes` ADD CONSTRAINT `FK_ee98d772a65959a5d42c7627574` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_likes` ADD CONSTRAINT `FK_abbd506a94a424dd6a3a68d26f4` FOREIGN KEY (`commentId`) REFERENCES `comments`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_following_users` ADD CONSTRAINT `FK_f257a3163914a2e398d7bfa8001` FOREIGN KEY (`usersId_1`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_following_users` ADD CONSTRAINT `FK_4a5ef04f52de98c49916c8798ec` FOREIGN KEY (`usersId_2`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_following_users` DROP FOREIGN KEY `FK_4a5ef04f52de98c49916c8798ec`");
        await queryRunner.query("ALTER TABLE `users_following_users` DROP FOREIGN KEY `FK_f257a3163914a2e398d7bfa8001`");
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_b2f0366aa9349789527e0c36d97`");
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_df951a64f09865171d2d7a502b1`");
        await queryRunner.query("ALTER TABLE `comment_likes` DROP FOREIGN KEY `FK_abbd506a94a424dd6a3a68d26f4`");
        await queryRunner.query("ALTER TABLE `comment_likes` DROP FOREIGN KEY `FK_ee98d772a65959a5d42c7627574`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_4548cc4a409b8651ec75f70e280`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_e44ddaaa6d058cb4092f83ad61f`");
        await queryRunner.query("ALTER TABLE `posts` DROP FOREIGN KEY `FK_c5a322ad12a7bf95460c958e80e`");
        await queryRunner.query("ALTER TABLE `post_likes` DROP FOREIGN KEY `FK_13cc94053492d02340fe803ca43`");
        await queryRunner.query("ALTER TABLE `post_likes` DROP FOREIGN KEY `FK_6999d13aca25e33515210abaf16`");
        await queryRunner.query("DROP INDEX `IDX_4a5ef04f52de98c49916c8798e` ON `users_following_users`");
        await queryRunner.query("DROP INDEX `IDX_f257a3163914a2e398d7bfa800` ON `users_following_users`");
        await queryRunner.query("DROP TABLE `users_following_users`");
        await queryRunner.query("DROP INDEX `IDX_b2f0366aa9349789527e0c36d9` ON `users_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_df951a64f09865171d2d7a502b` ON `users_roles_roles`");
        await queryRunner.query("DROP TABLE `users_roles_roles`");
        await queryRunner.query("DROP TABLE `comment_likes`");
        await queryRunner.query("DROP TABLE `comments`");
        await queryRunner.query("DROP TABLE `posts`");
        await queryRunner.query("DROP INDEX `IDX_450a05c0c4de5b75ac8d34835b` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `post_likes`");
        await queryRunner.query("DROP TABLE `black_listed_tokens`");
    }

}
