import {
  Entity,
  PrimaryGeneratedColumn,
  Column, ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./user";
import { Comment } from "./comment";
import { PostLikes } from "./post-like";
@Entity('posts')
export class PostEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { length: 50 })
  title: string;

  @Column('nvarchar', { length: 500 })
  content: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  version: number;

  @Column()
  isLocked: boolean;

  @Column()
  isDeleted: boolean;

  @Column()
  isFlagged: boolean;

  @ManyToOne(type => User, user => user.posts)
  author: Promise<User>;

  @OneToMany(type => Comment, comment => comment.post)
  comments: Promise<Comment[]>;

  @OneToMany(type => PostLikes, like => like.post)
  likes: Promise<PostLikes[]>;
}
