import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from "typeorm";

@Entity('black_listed_tokens')
export class BlackListedTokens {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    dateCreated: Date;

    @Column()
    token: string;
}
