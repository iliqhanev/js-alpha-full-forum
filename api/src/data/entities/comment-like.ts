import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from "typeorm";
import { Comment } from "./comment";
import { User } from "./user";

@Entity('comment_likes')
export class CommentLikes {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  vote: boolean;

  @ManyToOne(type => User, user => user.likes)
  author: Promise<User>;

  @ManyToOne(type => Comment, comment => comment.likes)
  comment: Promise<Comment>;
}
