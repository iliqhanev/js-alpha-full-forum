import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from "typeorm";
import { PostEntity } from "./post";
import { User } from "./user";

@Entity('post_likes')
export class PostLikes {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  vote: boolean;

  @ManyToOne(type => PostEntity, post => post.likes)
  post: Promise<PostEntity>;

  @ManyToOne(type => User, user => user.likes)
  author: Promise<User>;
}
