import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    JoinTable,
    ManyToMany,
    CreateDateColumn,
} from "typeorm";
import { PostEntity } from "./post";
import { Comment } from "./comment";
import { PostLikes } from "./post-like";
import { Role } from "./role";
@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ unique: true }) // 'nvarchar',
    name: string;

    @ManyToMany(type => Role, { eager: true })
    @JoinTable()
    roles: Role[];

    @Column({ unique: true })
    email: string;

    @Column({ unique: true })
    password: string;

    @CreateDateColumn()
    dateCreated: Date;

    @Column()
    isDeleted: boolean;

    @Column()
    isUnfriended: boolean;

    @Column()
    isBanned: boolean;

    @Column()
    bannedReason: string;

    @OneToMany(type => PostEntity, post => post.author)
    posts: Promise<PostEntity[]>;

    @OneToMany(type => Comment, comment => comment.author)
    comments: Promise<Comment[]>;

    @OneToMany(type => PostLikes, like => like.author)
    likes: Promise<PostLikes[]>;

    @JoinTable()
    @ManyToMany(type => User, user => user.followers)
    following: Promise<User[]>;

    @ManyToMany(type => User, user => user.following)
    followers: Promise<User[]>;
}
