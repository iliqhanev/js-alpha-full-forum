import {
  Entity, Column,
  CreateDateColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm";
import { PostEntity } from "./post";
import { User } from "./user";
import { CommentLikes } from "./comment-like";
import { Optional } from "@nestjs/common";
@Entity('comments')
export class Comment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { length: 500 })
  content: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  version: number;

  @UpdateDateColumn()
  updated: Date;

  @Column({ default: false })
  @Optional()
  isLocked: boolean;

  @Column({ default: false })
  @Optional()
  isDeleted: boolean;

  @Column({ default: false })
  @Optional()
  isFlagged: boolean;

  @ManyToOne(type => PostEntity, post => post.comments)
  post: Promise <PostEntity>;

  @ManyToOne(type => User, user => user.comments)
  author: Promise<User>;

  @OneToMany(type => CommentLikes, like => like.comment)
  likes: Promise<CommentLikes[]>;
}
