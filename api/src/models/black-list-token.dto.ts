import { IsString } from "class-validator";

export class BlackListTokenDTO {
    @IsString()
    token: string;
}
