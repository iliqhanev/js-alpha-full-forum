import { Expose } from "class-transformer";
import { IsEmail } from "class-validator";

export class ReturnFriendsDTO {
    @Expose()
    name: string;

    @Expose()
    email: string;

    @Expose()
    dateCreated: Date;
}
