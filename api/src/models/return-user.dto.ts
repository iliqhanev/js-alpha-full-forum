import { Expose } from 'class-transformer';
import 'class-validator';
import { User } from '../data/entities/user';

export class ReturnUserDTO {
    @Expose()
    name: string;

    @Expose()
    email: string;

    @Expose()
    dateCreated: Date;

    @Expose()
    following: Promise<User[]>;
}
