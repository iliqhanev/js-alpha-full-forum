import { CommentLikes } from './../data/entities/comment-like';
import { Expose } from 'class-transformer';
import { User } from '../data/entities/user';
import 'class-validator';

export class ReturnCommentDTO {

  @Expose()
  id: string;

  @Expose()
  author: string;

  @Expose()
  created: Date;

  @Expose()
  updated: Date;

  @Expose()
  content: string;

  @Expose()
  commentLikes: string;

  @Expose()
  commentDislikes: string;
}
