import { Expose, Type } from 'class-transformer';
import { User } from '../data/entities/user';
import 'class-validator';

export class ReturnPostDTO {

    @Expose()
    id: string;

    @Expose()
    author: string;

    @Expose()
    title: string;

    @Expose()
    content: string;

    @Expose()
    created: Date;

    @Expose()
    postLikes: string; // TODO number???

    @Expose()
    postDislikes: string; // TODO number???

    @Expose()
    isFlagged: boolean;

    @Expose()
    isLocked: boolean;
}
