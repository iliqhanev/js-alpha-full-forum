import { Allow, IsString } from "class-validator";

export class UpdatePostDTO {
    @IsString()
    content: string;

    @Allow()
    title?: string;
}
