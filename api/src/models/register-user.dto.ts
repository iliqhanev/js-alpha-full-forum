import { Length, IsEmail, IsString } from "class-validator";

export class RegisterUserDTO {
    @IsString()
    @Length(3, 20)
    name: string;

    @IsString()
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsString()
    @IsEmail()
    email: string;
}
