import { IsString, Length, Matches } from "class-validator";

export class UpdateCommentDTO {
  @IsString()
  @Length(3, 500)
  content: string;
}
