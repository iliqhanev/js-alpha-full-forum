import { Length, IsEmail, IsString, Matches } from 'class-validator';

export class UserLoginDTO {
    @IsString()
    @Length(3, 20)
    name: string;

    @IsString()
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsString()
    @IsEmail()
    email: string;
}
