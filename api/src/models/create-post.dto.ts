import { Length } from "class-validator";

export class CreatePostDTO {
    @Length(3, 50)
    title: string;

    @Length(3, 500)
    content: string;
}
