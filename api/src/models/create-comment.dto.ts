import { Length, IsString } from "class-validator";

export class CreateCommentDTO {
    @IsString()
    @Length(3, 500)
    content: string;
}
