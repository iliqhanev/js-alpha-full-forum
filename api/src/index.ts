import "reflect-metadata";
import { createConnection, LessThan, MoreThanOrEqual } from "typeorm";
import { plainToClass } from 'class-transformer';
import { ReturnUserDTO } from "../src/models/return-user.dto";
import { ReturnPostDTO } from '../src/models/return-post.dto';
import { User } from "./data/entities/user";
import { PostEntity } from "./data/entities/post";
import { Comment } from "./data/entities/comment";
import { PostLikes } from "./data/entities/post-like";
import { CommentLikes } from "./data/entities/comment-like";
import { Length } from "class-validator";

const main = async () => {
  const connection = await createConnection();
  const manager = connection.manager;

  const userRepo = manager.getRepository(User);
  const postRepo = manager.getRepository(PostEntity);
  const commentRepo = manager.getRepository(Comment);
  const postLikeRepo = manager.getRepository(PostLikes);
  const commentLikeRepo = manager.getRepository(CommentLikes);

  // const tenH = new Date().valueOf(); // - (3600 * 1000);
  // // tslint:disable-next-line:no-console
  // console.log(tenH);
  // const someUsers = await userRepo.find({
  //   where: {
  //     // name: 'Goro',
  //     // dateCreated: LessThan('2019-05-18 12:27:39.140732'), // ??? numbers?
  //     dateCreated: LessThan(tenH),
  //     // name: Length(4), // ???
  //   },
  // });
  // // tslint:disable-next-line:no-console
  // console.log(someUsers);

  // const joro = await userRepo.findOne({
  //   where: {
  //     name: 'Joro',
  //   },
  // });
  // const joroPost2 = new PostEntity();
  // joroPost2.author = Promise.resolve(joro);
  // joroPost2.title = 'Yes';
  // joroPost2.content = 'Ooooo Yeeeee...';
  // joroPost2.isLocked = false;
  // joroPost2.isDeleted = false;
  // await postRepo.save(joroPost2);

  // 1
  // const userYana = await userReposirory.findOne({
  //   where: {
  //     name: 'Yana'
  //   }
  // });

  // const returnUser = plainToClass(ReturnUserDTO, userYana, {
  //   excludeExtraneousValues: true
  // });

  // console.log(userYana);

  // console.log(returnUser); //ReturnUserDTO { name: 'Yana' }
  // console.log(returnUser.name); //Yana
  // console.log(returnUser.id); //undefined
  // 1

  // 2
  // const firstYanaPost = await postReposirory.findOne({
  //   where: {
  //     author: userYana
  //   }
  // });

  // const returnPost = plainToClass(ReturnPostDTO, firstYanaPost, {
  //   excludeExtraneousValues: true
  // });

  // console.log(returnPost.author.name); //undefinet
  // console.log(returnPost.title);
  // console.log(returnPost.content);
  // console.log(returnPost.created.toLocaleTimeString());
  // console.log(returnPost.created.toLocaleDateString());
  // 2

  // 3
  // const userJoro = await userReposirory.findOne({
  //   where: { name: 'Joro' }
  // });

  // const userPeter = await userReposirory.findOne({
  //   where: { name: 'Peter' }
  // });

  // const userPipi = await userReposirory.findOne({
  //   where: { name: 'Pipi' }
  // });

  // userJoro.followers = Promise.resolve([userYana, userPipi, userPeter]);
  // await userReposirory.save(userJoro);
  // console.log(await userJoro.followers);
  // userPipi.following = Promise.resolve([userYana, userPeter]);
  // await userReposirory.save(userPipi);
  // console.log(await userPipi.followers);
  // console.log(await userYana.followers);
  // console.log(await userPeter.followers);

  // userPipi.followers = Promise.resolve([userJoro]);
  // await userReposirory.save(userPipi);
  // console.log(await userPipi.followers);
  // console.log(await userPeter.followers);
  // 3

  // const postHello = await postReposirory.findOne({
  //   where: {
  //     title: 'Hi!'
  //   }
  // })

  // postHello.comment = Promise.resolve([]);
  // console.log(posthello);
  // console.log('///////////////////////////////');

  // console.log(await postHello)

  // const pesho = new User();
  // pesho.name = 'pesho121';
  // pesho.dateCreated = new Date;
  // pesho.email = 'ax@abv.bg'
  // pesho.isBanned = false;
  // await userReposirory.save(pesho);

  // const foundPipi = await userReposirory.findOne({
  //   where: {
  //     name: 'Pipi',
  //   },
  // });

  // const pipiPosts = await foundPipi.posts;
  // console.log(pipiPosts);

  // const foundJoro3 = await userRepo.findOne({
  //   where: {
  //     name: 'Joro3',
  //   },
  // });

  // foundPesho.following = Promise.resolve([foundPipi])
  // const updatedPesho = await userReposirory.save(foundPesho);
  // console.log(await updatedPesho.followers);
  // console.log(await updatedPesho.following);

  // const like1 = new PostLikes;
  // like1.author = foundPesho;
  // like1.vote = true;
  // like1.post = pipiPost;
  // await likeRepository.save(like1);

  // const like2 = new PostLikes;
  // like2.author = foundPesho;
  // like2.vote = true;
  // like2.post = pipiPost;
  // await likeRepository.save(like2);

  connection.close();
};

// tslint:disable-next-line:no-console
main().catch(console.error);

// ***************************************************************************

  // const foundUser = await manager.find(User,
  //   { firstName: 'Ivan' });
  //   console.log(foundUser);

  // const foundUser = await manager.find(User);
  //   console.log(foundUser);

  // Repository
  // const userReposirory = manager.getRepository(User);
  // const foundUser = await userReposirory
  //   .find({ id: '848ecc01-b3c0-4247-922a-c57c0d025b12' });
  // console.log(foundUser);

  // const userReposirory = manager.getRepository(User);
  // const invalidUser = await userReposirory.save({
  //   firstName: 'obo',
  //   lastName: 'obolele',
  //   age: 37
  // });
  // const findObo = await userReposirory.find({firstName: 'obo'});
  // console.log(findObo);

  // const userReposirory = manager.getRepository(User);
  // const delObo = await userReposirory.delete({firstName: 'obo'});
  // const users = await userReposirory.find();
  // console.log(users);

  // const newUser = userReposirory.create({lastName: 'Danny'});
  // const users = await userReposirory.find({lastName: 'Danny'});
  // console.log(newUser);
  // console.log(users);

  // const newUser = userReposirory
  //   .create({ firstName: 'Yana', lastName: 'Dudeck', age: 15 });
  // const newUser2 = userReposirory
  //   .create({ firstName: 'Danny', age: 14 });
  // const users = await userReposirory.save(newUser, newUser2);

  // console.log(newUser);
  // console.log(newUser2);
  // console.log(users);

  // const nodudeck = await userReposirory.delete({ lastName: 'Dudeck' });
  // console.log(nodudeck);

  // const users = await userReposirory.find({});
  // return plainToClass(ReturnUserDTO, users);

  // await userReposirory
  //   .update('848ecc01-b3c0-4247-922a-c57c0d025b12', { firstName: 'Obo' });
  // const findUser = await userReposirory.find({ firstName: 'Obo' });
  // console.log(findUser);

  //// dinamic creation
  // const newUser = userReposirory
  //   .create({ firstName: 'Yana', lastName: 'Dudeck', age: 15 });
  // await userReposirory.save(newUser);

  // const newUser = userReposirory
  //   .create({ firstName: 'Pipi', lastName: 'Long', age: 8 });
  // await userReposirory.save(newUser);

  // const newPost = new Post;
  // newPost.dateCreated = new Date();
  // newPost.title = 'title2';
  // newPost.content = 'tra lala la la';
  // newPost.autor = newUser;
  // await postReposirory.save(newPost);

  // console.log(newUser);
  // console.log(newPost);

  // await userReposirory
  //    .update('11dd24db-f464-47a4-93e8-9fa84df79c9f', { firstName: 'Pipilota' });

  // const pipiUser = await userReposirory
  //   .find({ id: '10b13315-3936-4663-8113-a25587b6828b' });
  // console.log(newUser);

  // const newPost = new Post;
  // newPost.dateCreated = new Date();
  // newPost.title = 'title3';
  // newPost.content = 'tra lala la la yyyy';
  // newPost.autor = pipiUser[0];

  // const newPost2 = new Post;
  // newPost.dateCreated = new Date();
  // newPost.title = 'title5';
  // newPost.content = 'Yahoooo';
  // newPost.autor = pipiUser[0];

  // await postReposirory.save(newPost, newPost2);

  // const pipiPosts = await postReposirory.find({ autor: pipiUser[0] });
  // console.log(pipiPosts);

  // const pipiUser = await userReposirory.find({ firstName: 'Pipi' });
  // console.log(pipiUser);
  // console.log(pipiUser[0].posts);
  // console.log(pipiUser[0].posts[0].autor); //undefinet

  // //pipiUser[0].posts[0].autor

  // const foundPost = await postReposirory
  // .find({id: 'b25f8e17-8253-4bcd-8232-0e3c9a7a0bc4'});
  // console.log(foundPost);
  // console.log(foundPost[0].autor); //undefined

  // foundPost[0].autor

  // const userYana = await userReposirory.findOne({ firstName: 'Yana' });
  //// console.log(userYana);
  // const yanaPost = new Post;
  // yanaPost.autor = userYana;
  // yanaPost.content = 'I am Yana!';
  // yanaPost.dateCreated = new Date();
  // yanaPost.title = 'Hi';
  // await postReposirory.save(yanaPost);

  // const userYana = await userReposirory.findOne({ firstName: 'Pipi' });
  // //  const yanaId = userYana.id.toString();
  // //  console.log(yanaId);
  // const yanaPosts = await postReposirory.find({ autor: userYana });
  // console.log(yanaPosts);

  // const findPipi = await manager.findOne(User, { firstName: 'Pipi' });

  // const posts = await findPipi.posts;
  // console.log(posts);
  // console.log("*********************");

  // const pipiPost = new Post;
  // pipiPost.autor = findPipi;
  // pipiPost.content = 'I am Pipi!';
  // pipiPost.dateCreated = new Date();
  // pipiPost.title = 'Hello';
  // //await postReposirory.save(pipiPost);

  // posts.push(pipiPost);
  // findPipi.posts = Promise.resolve(posts);
  // await manager.save(findPipi);

  // console.log(await findPipi.posts);

  // const pipiPosts = await postReposirory.find({autor: findPipi});
  // console.log(pipiPosts);

  // const pipi = await manager.findOne(User, {firstName: 'Pipi'});
  // const pipiPosts = await postReposirory.find({autor: pipi});
  // console.log(pipiPosts);

  // //@Req() req: User;
  // //req.user

  // const findPost = await postReposirory
  // .findOne({id: '58ba0e8e-7e8f-4c36-8f98-e37bc3c1381f'});
  // console.log(findPost);

  // const autor = await userReposirory
  // .findOne({id: '848ecc01-b3c0-4247-922a-c57c0d025b12'});
  // console.log(autor);

  // const comment = new Comment;
  // comment.content = 'really cool';
  // comment.created = new Date();
  // comment.autor = autor;
  // comment.post = findPost;

  // await commentRepository.save(comment);

  // const autor = await userReposirory
  //   .findOne({ id: '848ecc01-b3c0-4247-922a-c57c0d025b12' });
  // console.log(autor);
