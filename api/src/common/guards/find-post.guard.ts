import { Injectable, CanActivate, ExecutionContext, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from '../../data/entities/post';

@Injectable()
export class FindPostGuard implements CanActivate {
    constructor(
        @InjectRepository(PostEntity) private readonly postsRepo: Repository<PostEntity>) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const postId = request.params.postId;
        const found = await this.postsRepo.findOne({ id: postId });

        if (!found) {
            throw new NotFoundException(`Post with id ${postId} does not exist!`);
        }

        return true;
    }
}
