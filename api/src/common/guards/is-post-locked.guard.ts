import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from '../../data/entities/post';
import { UserRole } from '../enums/user-role.enum';

@Injectable()
export class IsPostLockedGuard implements CanActivate {
    constructor(
        @InjectRepository(PostEntity) private readonly postsRepo: Repository<PostEntity>) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const postId = request.params.id;

        const isAdmin = request.user.roles
            .map(role => role.name)
            .includes(UserRole.Admin);
        const isLocked = await this.postsRepo.findOne({ id: postId, isLocked: true });

        if (isLocked) {
            if (isAdmin) {
                return true;
            } else {
                throw new ForbiddenException(`The post is locked for changes, try again later!`);
            }
        }
    }
}
