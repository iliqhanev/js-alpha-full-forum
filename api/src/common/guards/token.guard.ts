import { Injectable, CanActivate, ExecutionContext, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BlackListedTokens } from '../../data/entities/black-list-tokens';
import { Repository } from 'typeorm';

@Injectable()
export class TokenGuard implements CanActivate {
    constructor(
        @InjectRepository(BlackListedTokens) private readonly tokensRepo: Repository<BlackListedTokens>) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const token = request.headers.authorization;
        const isBlackListed = await this.tokensRepo.findOne({ token });

        if (isBlackListed) {
            throw new BadRequestException(`You have to log in to continue!`);
        }

        return true;
    }
}
