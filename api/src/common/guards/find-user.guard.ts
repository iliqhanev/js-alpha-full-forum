import { Injectable, CanActivate, ExecutionContext, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../data/entities/user';

@Injectable()
export class FindUserGuard implements CanActivate {
    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const userName = request.params.friendName;
        const found = await this.userRepo.findOne({ name: userName });
        if (!found) {
            throw new NotFoundException(`User with name '${userName}' does not exist!`);
        }

        return true;
    }
}
