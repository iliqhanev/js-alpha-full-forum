import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from '../../data/entities/post';
import { UserRole } from '../enums/user-role.enum';

@Injectable()
export class AuthorAdminGuard implements CanActivate {
    constructor(
        @InjectRepository(PostEntity) private readonly postsRepo: Repository<PostEntity>) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const postId = request.params.id;
        const user = request.user;

        const isAdmin = request.user.roles
            .map(role => role.name)
            .includes(UserRole.Admin);
        const isAuthor = await this.postsRepo.findOne({ id: postId, author: user });

        if (isAdmin || isAuthor) {
            return true;
        } else {
            throw new ForbiddenException(`You can not edit this post.`);
        }
    }
}
