import { createConnection, Repository } from 'typeorm';
import { User } from '../src/data/entities/user';
import { Role } from './data/entities/role';
import { UserRole } from '../src/common/enums/user-role.enum';
import * as bcrypt from 'bcrypt';
// SENSITIVE DATA ALERT! - normally the admin credentials should not be present in the public repository, but for now we will roll with it - you can think about how to do it better
// run: `npm run seed` to seed the database

const seedRoles = async connection => {
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

    const roles: Role[] = await rolesRepo.find();
    if (roles.length) {
        console.log('The DB already has roles!');
        return;
    }

    const rolesSeeding: Array<Promise<Role>> = Object.keys(UserRole).map(
        async (roleName: string) => {
            const role: Role = rolesRepo.create({ name: roleName });
            return await rolesRepo.save(role);
        },
    );
    await Promise.all(rolesSeeding);

    console.log('Seeded roles successfully!');
};

const seedAdmin = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

    const admin = await userRepo.findOne({
        where: {
            name: 'admin',
        },
    });

    if (admin) {
        console.log('The DB already has an admin!');
        return;
    }

    const adminRole: Role = await rolesRepo.findOne({ name: UserRole.Admin });
    if (!adminRole) {
        console.log('The DB does not have an admin role!');
        return;
    }

    const newAdmin: User = userRepo.create({
        isDeleted: false,
        isUnfriended: false,
        isBanned: false,
        bannedReason: 'notBanned',
        name: 'admin',
        email: 'admin@abv.bg',
        password: await bcrypt.hash('admin', 10),
        roles: [adminRole],
    });

    const basicRole: Role = await rolesRepo.findOne({ name: UserRole.Basic });
    if (!basicRole) {
        console.log('The DB does not have an basic role!');
        return;
    }

    const newBasic: User = userRepo.create({
        isDeleted: false,
        isUnfriended: false,
        isBanned: false,
        bannedReason: 'notBanned',
        name: 'basic',
        email: 'basic@abv.bg',
        password: await bcrypt.hash('basic', 10),
        roles: [basicRole],
    });

    const newBasic1: User = userRepo.create({
        isDeleted: false,
        isUnfriended: false,
        isBanned: false,
        bannedReason: 'notBanned',
        name: 'basic1',
        email: 'basic1@abv.bg',
        password: await bcrypt.hash('basic1', 10),
        roles: [basicRole],
    });

    const newBasic2: User = userRepo.create({
        isDeleted: false,
        isUnfriended: false,
        isBanned: false,
        bannedReason: 'notBanned',
        name: 'basic2',
        email: 'basic2@abv.bg',
        password: await bcrypt.hash('basic2', 10),
        roles: [basicRole],
    });

    await userRepo.save(newBasic);
    await userRepo.save(newBasic1);
    await userRepo.save(newBasic2);
    await userRepo.save(newAdmin);
    console.log('Seeded admin successfully!');
};

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();

    await seedRoles(connection);
    await seedAdmin(connection);

    await connection.close();
    console.log('Seed completed!');
};

seed().catch(console.error);
