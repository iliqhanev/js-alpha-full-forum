import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from './common/guards/roles.guard';
import { UserRole } from './common/enums/user-role.enum';
import { Roles } from './common/decorators/roles.decorator';
import { User } from './common/decorators/user.decorator';
import { PostsService } from './posts/posts.service';

@Controller()
export class AppController {
  constructor(private readonly postsService: PostsService) { }

  @Get()
  async publicPartPosts() {
    return {
      message: `This is the public part, you can only read the posts!`,
      posts: await this.postsService.allPosts(),
    };
  }
}
